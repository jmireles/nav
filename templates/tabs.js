// gitlab.com/jmireles/nav/templates/tabs.js
const tabs = {
	save: false,
	show: (key, pos)=> {
		if (pos == undefined) {
			pos = 0
		}
		document.getElementsByName(key).forEach(n => {
			if (n.id == key + "-" + pos) {
				n.className = key + "-tab-sel";
			} else {
				n.className = key + "-tab-unsel";
			}
		})
		document.getElementsByName(key + "-tab").forEach(n => {
			if (n.id == key + "-tab-" + pos) {
				n.className = "shown";
			} else {
				n.className = "hidden";
			}
		})
		if (tabs.save && localStorage) {
			localStorage['tab-' + key] = pos;
		}
	},
	init: (keys)=> {
		tabs.save = true;
		if (localStorage) {
			keys.forEach(key => {
				tabs.show(key, localStorage['tab-' + key]);
			})
		}
	}
}
