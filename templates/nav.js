// gitlab.com/jmireles/nav/templates/nav.js
const storage = {
	set: (k,v)=> { 
		localStorage["{{ .Clean }}-" + k] = v; 
	},
	get: (k)=> {
		return localStorage["{{ .Clean }}-" + k]; 
	},
}

const postReq = (values, params, resp, dom)=> {
	popup.init()
	const x = new XMLHttpRequest();
	const update = (map)=> {
		for (const k in map.innerHTML || {}) {
			document.getElementById(k).innerHTML = map.innerHTML[k];
		}
	}
	update(resp);
	x.onreadystatechange = function() {
		if (x.readyState == XMLHttpRequest.DONE) {
			if (x.status == 200) {
				if (x.responseText == '') {
					return;
				}
				try {
					const ok = JSON.parse(x.responseText)
					update(ok)
					if (ok.error) {
						popup.set(ok.error, "popup error");
					} else if (ok.popup) {
						popup.set(ok.popup, "popup help");
					}
				} catch (e) {
					console.log("Error " + e + " '" + x.responseText + "'");	
				}
			} else if (x.status >= 400) {
				err("Error " + x.status);
			}
		}
	}
	x.onerror = function() {
		err("Connection refused");
	}
	x.open("POST", "{{ .Paths.PostReq }}", true);
	values.params = {};
	params.forEach(param => {
		values.params[param] = document.getElementById(param).value;
	})
	if (dom) {
		values.dom = {
			id: dom.getAttribute('id'),
			int: dom.getAttribute('data-int'),
			text: dom.innerHTML,
		};
	}
	values.prefs = prefs.read();
	x.send(JSON.stringify(values));
}

let s;
const n = { o:0, c:0 };
const tx  = (go)=> { 
	if (s) {
		s.send(JSON.stringify({ go: go, prefs: prefs.read() })); 
	}
}

const rxT = (t) => {
	for (const k in t) {
		const e = document.getElementById(k);
		if (e) {
			e.innerHTML = t[k];
		}
	}
}

const K = ({{ .AttrsKeys }});

const rxA = (a) => {
	for (const id in a) {
		const e = document.getElementById(id);
		if (e) {
			for (const k in a[id]) {
				if (k == "{{ .AttrText }}") {
					e.innerHTML = a[id][k];
				} else if (k == "{{ .AttrClass }}") {
					e.className = a[id][k];
				} else {
					e.setAttribute(K[k], a[id][k]);
				}
			}
		}
	}
}

const ok = (t)=> {
	prefs.write(''); 
	rxT({
		"{{ .Ids.Conn }}" : `<span class="ok" title="opens">` + t + `</span>`
	}); 
}

const err = (t)=> {
	prefs.write('');
	rxT({
		"{{ .Ids.Conn }}":"<span class='err' title='closings'>" + t + "</span>"
	});
}

const conn = ()=> {
	const protocol = (window.location.protocol == "https:") ? "wss:" : "ws:";
	const uri = protocol + "//" + location.host;
	s = new WebSocket(uri + "{{ .Paths.Websocket }}");

	s.onopen = (text)=> {
		n.o++;
		n.c = 0;
		ok(n.o);
		tx(storage.get("go") || "0");
	}
	s.onerror = (text)=> {
		err(text);
	}
	s.onmessage = (text)=> {
		try {
			const rx = JSON.parse(text.data);
			if (rx.texts) {
				rxT(rx.texts);
			}
			if (rx.attrs) {
				rxA(rx.attrs);
			}
		} catch (e) {
			console.log("ERROR text.data not JSON '" + text.data + "'");
		}
	}
	s.onclose = (e)=> {
		if (popup) {
			popup.close();
		}
		let sec = ({{ .Timeout }});
		const head = "<div class='err'>Close-code: " + e.code + ", timeout: " + sec + "s</div>"
		rxT({
			"{{ .Ids.Head }}":head,
			"{{ .Ids.Body }}":""
		});
		n.c++;
		err(n.c);
		s = null;
		setTimeout(conn, sec*1000);
	}
}

document.addEventListener('click', function(ev) {
	if (!ev.target) {
		return;
	}
	if (!ev.target.closest('.go')) {
		return;
	}
	const id = ev.target.id;
	storage.set("go", id);
	tx(id);
}, false);

const prefs = {
	write: (html,reload)=> {
		document.getElementById("{{ .Ids.Prefs }}").innerHTML = html;
		if (reload) {
			location.reload();
		}
	},
	change: (key, value)=> {
		storage.set(key, value);
		prefs.open();
	},
	open: ()=> {
		const x = new XMLHttpRequest();
		x.onreadystatechange = function() {
			if (x.readyState == XMLHttpRequest.DONE) {
				if (x.status == 200) {
					prefs.write(x.responseText);
				} else if (x.status >= 400) {
					prefs.write("");
					err("Error " + x.status);
				}
			}
		}
		x.onerror = function() {
			err("Connection refused");
		}
		x.open(`POST`, `{{ .Paths.PostPrefs }}`, true);
		x.send(JSON.stringify({
			lang: storage.get("lang"),
			units: storage.get("units"),
		}));
	},
	read: ()=> {
		const u = storage.get("units") || "";
		const l = storage.get("lang") || "";
		return u + ":" + l;
	}
};

document.addEventListener("keyup", (e) => {
    if (e.key == "Home") {
        prefs.open();
    } else if (e.key == "Escape") {
    	prefs.write("");
    	if (popup) {
    		popup.close();
    	}
    }
});

const popup = {
	set: (html, className)=> {
		const p = document.getElementById("{{.Ids.Popup}}");
		p.innerHTML = html;
		p.className = className;
		return p
	},
	init: ()=> {
		const p = popup.set("", "hidden")
	},
	close: ()=> {
		popup.set("", "hidden");
	}
};
