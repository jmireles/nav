// gitlab.com/jmireles/nav/templates/cfg.js
const cfg = {
	showG: (g)=> {
   		popup.close();
		document.getElementsByName('cfg-gb').forEach(n => {
			if (n.id == "cfg-gb-" + g) {
				n.className = "cfg-sel";
			} else {
				n.className = "cfg-unsel";
			}
		})
		document.getElementsByName('cfg-g').forEach(n => {
			if (n.id == "cfg-g-" + g) {
				n.className = "shown";
			} else {
				n.className = "hidden";
			}
		})
	},
	updateParam: (g, f, dataInt, text) => {
		const e = document.getElementById("cfg-param-" + g + "-" + f);
		if (e) {
			e.className = "cfg-value cfg-edited";
			e.setAttribute("data-int", dataInt);
			e.innerHTML = text;
			popup.close()
		}
		if (g != 1) {
			const e2 = document.getElementById("cfg-editor");
			if (e2) {
				e2.className = "cfg-flex visible";
			}
		}
		popup.close();
	},
	changeNumber: (btn, g, f, up)=> {
		const button = window.event.button
		const e = document.querySelector("#cfg-editor-" + g + "-" + f);
		if (!e) {
			return;
		}
		let step = 1;
		const max = parseInt(e.dataset.max);
		const min = parseInt(e.dataset.min);
		const s = (e.dataset.factor != null) ? {
			f: parseFloat(e.dataset.factor),
			b: parseInt(e.dataset.base),
			d: parseInt(e.dataset.decimals)
		} : null;
		const change = ()=> {
		    if (button != 0) {
	        	return false;
	    	}
			let ok = true;
			const was = parseInt(e.dataset.val);
			let v = 0;
			if (up) {
				const val = was + step;
				ok = (val < max);
				v = ok ? val : max;
			} else {
				const val = was - step;
				ok = (val > min);
				v = ok ? val : min;
			}
			e.dataset.val = v;
			if (s) {
				e.innerHTML = parseFloat(s.f * (v - s.b)).toFixed(s.d);
			} else {
				e.innerHTML = v;
			}
			return ok;
		}
		let n = 0;
		let c = 0;
		let m = 10;
		const auto = ()=> {
			n++;
			if (n > 100 && n % m == 0) {
				if (change() == false) {
					clearInterval(mi);
					return;
				}
				c++;
				if (c == 20) {
					m = 2;
				} else if (c == 60) {
					m = 1;
					step *= 2;
				} else if (c == 120) {
					step *= 2;
				}
			}
		}
		const mi = setInterval(auto, 10);
		btn.onmouseup = function() {
			clearInterval(mi);
			if (c==0) {
				change();
			}
		}
	},
	updateParamNumber: (g, f)=> {
		const e = document.getElementById("cfg-editor-" + g + "-" + f);
		if (e) {
			const v = e.dataset.val;
			cfg.updateParam(g, f, parseInt(v), e.innerHTML);
		}
	},
	readParamNumber: (g, f)=> {
		const r = {}
		const e = document.getElementById("cfg-editor-" + g + "-" + f); if (e) {
			r[g + "-" + f] = parseInt(e.dataset.val);
		}
		return r
	},
	values: ()=> {
		const values = {}
		const s = "cfg-param-".length
		document.getElementsByName('cfg-param').forEach(n => {
			try {
				const k = n.getAttribute("id").substring(s)
				const v = n.getAttribute("data-int")
				values[k] = parseInt(v)
			} catch (e) {
				
			}
		})
		return values;
	},
	read: (v)=> {
		v.values = cfg.values();
		postReq(v, [], {});
	},
	saveParams: (v)=> {
		v.values = cfg.values();
		postReq(v, [], {});
	},
	saveParam: (v, g, f)=> {
		v.values = cfg.readParamNumber(g, f);
		postReq(v, [], {});
		popup.close();
	},
	cancel: ()=> {
		popup.close();
		rxT({"cfg-resp":""});
	}
};