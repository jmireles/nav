package nav

import (
	"net/http"
	"strings"
)

var localhosts = []string{"[::1]", "localhost", "127.0.0.1"}

func MiddlewareLocalhost(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for _, localhost := range localhosts {
			if strings.HasPrefix(r.RemoteAddr, localhost) {
				next.ServeHTTP(w, r)
				return
			}
		}
		w.Write(JsonError("remote address is not loopback"))
	})
}
