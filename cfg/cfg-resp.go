package cfg

import (
	"errors"
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

type resp struct {
	cfg   *Cfg
	post  *nav.ReqPost
	icon  dom.Icon
	icons dom.Icons
}

// PopupHelp returns a json response to display a popup with text help
func (r *resp) PopupHelp() *nav.JsonResp {
	if gf, err := r.newGroupField(); err != nil {
		return nav.NewJsonRespPopup("", err)
	} else {
		prefs := r.post.Prefs()
		title := gf.header(prefs)
		paragraphs := gf.i18nF.Help.Get(prefs.Lang)
		return PopupHelp(r.post, title, paragraphs)
	}
}

// PopupEdit returns a json response to display a popup with and editor
// control. There are two type of controls: options list and numeric.
func (r *resp) PopupEdit() *nav.JsonResp {
	if gf, err := r.newGroupField(); err != nil {
		return nav.NewJsonRespPopup("", err)
	} else {
		title := gf.header(r.post.Prefs())
		gs, fs := strconv.Itoa(gf.g), strconv.Itoa(gf.f)
		v := gf.field.val
		return PopupEdit(r.post, title, gs, fs, v)
	}
}

// PopupSave returns a json response to display a popup with save confirmation/cancel.
// User clicked "Save this" or "Save all" buttons
// under browser cfg-resp panel when at list a parameter was edited
func (r *resp) PopupSave() *nav.JsonResp {
	// pass javascript cfg.save method line, command, id and all
	// in there the DOM will be iterated to get the updated params
	// and then call reqPost to reach the server to write the box
	return PopupSave(r.post)
}

// HtmlResult returns HTML string with two rows.
// First row includes columns icon group-i18n and iff resfresh is true refresh/icons aligned to the right.
// Second row includes a text if ok is != "" or err (string or error types) aligned to the right.
func (r *resp) HtmlResult(refresh bool, ok string, err interface{}) string {
	xml := r.post.XML()
	r.header(refresh, nil)
	domBtnsFlex(xml, func() {
		domBtnW(xml, nil) // flex-1 moves next DOM elements to the right
		if ok != "" {
			domGet(xml, ok)
		} else if err != nil {
			switch err.(type) {
			case error:
				domError(xml, err.(error).Error())
			case string:
				domError(xml, err.(string))
			default:
				domError(xml, "?")
			}
		}
	})
	return xml.String()
}

// Returns HTML with icon group-i18n and params data
func (r *resp) HtmlResp(data []byte, icons dom.Icons) (string, bool) {
	if r.cfg.comms.resp != data[0] {
		// data[0] should matches this cfg expected response
		return "", false
	}
	// replace data[0] with get instead of correct resp
	data[0] = r.cfg.comms.get

	xml := r.post.XML()
	prefs := r.post.Prefs()
	// call line cfg read implementation
	params, err := r.cfg.gfms.mess.Read(data, prefs)
	if err != nil {
		domError(xml, "r.cfg.mess.Read:"+err.Error())
		return xml.String(), true
	}
	r.header(true, data) // pass data for profile param(s) if any

	domBtnsSaveBoxAll(r.post)
	xml.W0(`<hr/>`)
	// Row with tabs "icon-radio-buttons" groups translated
	// clicking hides/shows group's fields controls
	for g, group := range r.cfg.gfms.groups {
		if g > 0 {
			attrs := domAttrsGroupTab(g)
			_, gName := r.cfg.i18nGroup(group, prefs)
			xml.Div(attrs, gName)
		}
	}
	// tables hidden/show for each group's with fields controls
	for g, group := range r.cfg.gfms.groups {
		if g > 0 {
			r.groupTable(g, group, params)
		}
	}
	return xml.String(), true
}

// header displays in a row: First colum Icon-name of a cfg command. Then aligned to the right
// more columns are added, profile if available for the cfg and buttons for refresh and cancel.
func (r *resp) header(refresh bool, data []byte) {
	xml := r.post.XML()
	prefs := r.post.Prefs()
	domBtnsFlex(xml, func() {
		commandBtn := &domBtn{r.icon, r.cfg.i18n.Id.Get(prefs.Lang), domStyleNone, "", ""}
		domBtnW(xml, commandBtn)
		domBtnW(xml, nil) // flex-1 moves next DOM elements to the right
		if refresh {
			r.profileReadAndCancel(data) // right
		}
	})
}

func (r *resp) profileReadAndCancel(data []byte) {
	// include profile (if any), read button and cancel button
	req := r.post.Req()
	readJS := req.PostGet().JavascriptOnclick() // for no profiles only
	// only first group has profile info if any
	g, group := 0, r.cfg.gfms.groups[0]

	xml := r.post.XML()
	prefs := r.post.Prefs()
	if len(r.cfg.gfms.fields[group]) > 0 {
		if data == nil {
			data = make([]byte, 9) // TODO 9 is for certain comms not all check cfg!!!
		}
		if params, err := r.cfg.gfms.mess.Read(data, prefs); err != nil {
			domError(xml, "r.cfg.mess.Read"+err.Error())
			return
		} else {
			// insert param for profile
			r.groupTable(g, group, params)
		}
		// for profiles, change readJS call to let javascript
		// to include parameters editions in DOM, first one will be
		// the profile needed (inserted above)
		readJS = "cfg.read(" + req.PostGet().Values() + ")"
	}
	cancelJS, cancelI18n := "cfg.cancel()", r.post.I18nCancel()
	domBtnW(xml, &domBtn{IconGet, "", domStyleRead, readJS, r.post.I18nRead()})
	domBtnW(xml, &domBtn{IconCancel, "", domStyleCancel, cancelJS, cancelI18n})
}

// groupTable displays a table with parameters. There are four columns:
// an icon, a text with the name of the param and clickable to get a help popup with more info.
// Last two columns are the value of the param clickable for edition and a units optionally.
func (r *resp) groupTable(g int, group string, params *Params) {
	xml := r.post.XML()
	prefs := r.post.Prefs()
	req := r.post.Req()
	xml.W("table", domAttrsGroupTable(g), func() {
		for f, field := range r.cfg.gfms.fields[group] {
			xml.Tr(func() {
				// First column: icon
				xml.Td(nil, func() {
					if icon, ok := r.icons[field.id]; ok {
						xml.Icon(icon)
					}
				})
				// Second column: name (click to get help)
				xml.Td(nil, func() {
					attrs := domAttrsHelp(req, g+1, f+1)
					xml.Div(attrs, r.cfg.i18nField(group, field, prefs))
				})
				// Third column: params (click to edit TODO)
				xml.Td(nil, func() {
					if param, ok := params.m[field.id]; ok {
						post := nav.JsonPostEdit(req, g+1, f+1)
						attrs := domAttrsEditorNumber(post, g+1, f+1, param.number)
						xml.Div(attrs, param.scaled)
					}
				})
				// Fourth column: units
				xml.Td(nil, func() {
					if unit := field.val.unitLabel(prefs); unit != nil {
						attrs := domAttrsUnits(unit.Title)
						xml.Div(attrs, unit.Abbrev)
					}
				})
			})
		}
	})
}

func (r *resp) newGroupField() (*cfgGF, error) {
	req := r.post.Req()
	groups := r.cfg.gfms.groups
	g := int(req.Group - 1)
	if g < 0 || len(groups) < g {
		return nil, errors.New("Group pos out of range:" + strconv.Itoa(g))
	}
	group := groups[g]
	gI18n, ok := r.cfg.i18n.Groups[group]
	if !ok {
		return nil, errors.New("Group missing in i18n:" + group)
	}
	fields := r.cfg.gfms.fields[group]
	f := int(req.Field - 1)
	if f < 0 || len(fields) < f {
		return nil, errors.New("Field pos out of range:" + strconv.Itoa(f))
	}
	field := fields[f]
	fI18n, ok := gI18n.Fields[field.id]
	if !ok {
		return nil, errors.New("Field missing in i18n:" + strconv.Itoa(f))
	}
	return &cfgGF{&gI18n, field, &fI18n, g + 1, f + 1}, nil
}

type cfgGF struct {
	i18nG *I18nGroup
	field *field
	i18nF *I18nField
	g     int
	f     int
}

func (c *cfgGF) header(prefs *nav.Prefs) string {
	return c.i18nG.Id.Get(prefs.Lang) + ` | ` + c.i18nF.Id.Get(prefs.Lang)
}
