package cfg

import (
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

type Cfg struct {
	comms *Comms
	gfms  *GFMS
	i18n  *I18n
	sim   *sim
}

func NewCfg(comms *Comms, gfms *GFMS, i18n *I18n) *Cfg {
	cfg := &Cfg{
		comms: comms,
		gfms:  gfms,
		i18n:  i18n,
	}
	cfg.sim = newSim(gfms.simF, cfg)
	return cfg
}

func (c *Cfg) NewResp(post *nav.ReqPost, icon dom.Icon, icons dom.Icons) *resp {
	return &resp{c, post, icon, icons}
}

func (c *Cfg) ProfileReq(post *nav.ReqPost) []byte {
	values := post.Req().Values
	if len(values) == 0 {
		return nil
	}
	tx := make(map[string]int)
	for g, group := range c.gfms.groups {
		for f, field := range c.gfms.fields[group] {
			if g == 0 && f == 0 {
				key := strconv.Itoa(g+1) + "-" + strconv.Itoa(f+1)
				tx[field.id] = values[key]
			}
		}
	}
	return c.gfms.mess.ProfileReq(tx)
}

func (c *Cfg) SavePost(post *nav.ReqPost) error {
	values := post.Req().Values // numeric
	// convert map keys to field names
	tx := make(map[string]int)
	for g, group := range c.gfms.groups {
		for f, field := range c.gfms.fields[group] {
			key := strconv.Itoa(g+1) + "-" + strconv.Itoa(f+1)
			tx[field.id] = values[key]
		}
	}
	// call line cfg save implementation
	if bytes, err := c.gfms.mess.Save(tx); err != nil {
		return err
	} else {
		post.Save(c.comms.set, bytes)
		return nil
	}
}

// RespComplete parses cfg responses. Each response can be only a part of a whole response
// Returns cfg false if respComm is not found in Line's list of command responders
// Returns complete false when packet is not the last one, and true when is the last
// buffer is hold by the box been configured
func (c *Cfg) RespParse(part []byte, whole *[]byte) (complete bool) {
	complete = c.gfms.mess.Resp(c.comms.resp, part, whole)
	return
}

func (c *Cfg) SimResp(params url.Values) (bs []byte, e error) {
	return c.gfms.mess.SimTx(params)
}

func (c *Cfg) MessRead(data []byte) (*Params, error) {
	return c.gfms.mess.Read(data, nil)
}

func (c *Cfg) SimSave(bytes [][]byte) (map[string]string, error) {
	if params, err := c.gfms.mess.SimRx(c.comms.line, c.comms.set, c.comms.resp, bytes); err != nil {
		return nil, err
	} else {
		m := make(map[string]string)
		for k, v := range params.m {
			m[k] = v.scaled
		}
		return m, nil
	}
}

func (c *Cfg) SimControls(r *dom.XML, tab int) {
	c.sim.controls(r)
}

func (c *Cfg) i18nGroup(group string, prefs *nav.Prefs) (*I18nGroup, string) {
	if c.i18n == nil {
		// [] no Cfg defined
		return nil, "[" + group + "]"
	} else if g, ok := c.i18n.Groups[group]; !ok {
		// {} means group exists but no element is defined
		return nil, "{" + group + "}"
	} else {
		lang := nav.LangEn
		if prefs != nil {
			lang = prefs.Lang
		}
		// group translated
		return &g, g.Id.Get(lang)
	}
}

func (c *Cfg) i18nField(group string, field *field, prefs *nav.Prefs) string {
	if i18nG, _ := c.i18nGroup(group, prefs); i18nG == nil {
		return "[" + field.id + "]"
	} else if f, ok := i18nG.Fields[field.id]; !ok {
		return "{" + field.id + "}"
	} else {
		lang := nav.LangEn
		if prefs != nil {
			lang = prefs.Lang
		}
		return f.Id.Get(lang)
	}
}

// Param has a Text for displaying and option selected,
// a integer or a float number. Have an Int value which later
// is saved of transmitted as an array of bytes.
type Param struct {
	scaled string // string value (option, int1, int2)
	number int    // numeric value (byte, short, int)
}

// Label is a text with an abbreviation text and a title for description
// Used by real-time parameters and cfg parameters units.
type Label struct {
	Abbrev string
	Title  string
}

func NewLabel(abbrev, title string) *Label {
	return &Label{
		Abbrev: abbrev,
		Title:  title,
	}
}

type Params struct {
	prefs *nav.Prefs
	m     map[string]*Param
}

func NewParams(prefs *nav.Prefs) *Params {
	return &Params{prefs, make(map[string]*Param)}
}

func (p *Params) set(key string, value *Param) {
	p.m[key] = value
}

func (p *Params) String() string {
	var sb strings.Builder
	for k, v := range p.m {
		sb.WriteString(" " + k + "=" + v.scaled)
	}
	return sb.String()
}

type Comms struct {
	line     byte
	lineS    string
	get      byte
	set      byte
	resp     byte
	profiles []int
}

func NewComms(line byte, lineS string, get, set, resp byte, profiles []int) *Comms {
	return &Comms{line, lineS, get, set, resp, profiles}
}
