package cfg

// GFMS joins groups, fields, messages and simulation fields
type GFMS struct {
	groups []string
	fields map[string]Fields
	mess   Mess
	simF   []Fields
}

func NewGFMS(groups []string, fields map[string]Fields, mess Mess, simF []Fields) *GFMS {
	return &GFMS{
		groups: groups,
		fields: fields,
		mess:   mess,
		simF:   simF,
	}
}

type Inits []string

type Fields []*field

type field struct {
	id    string
	val   val // options or number
	inits Inits
}

func Field(id string, value val, inits Inits) *field {
	if len(inits) == 0 {
		panic("field inits length == 0")
	}
	return &field{id, value, inits}
}

func (f *field) init(profile int) string {
	if profile > 0 {
		pos := profile - 1
		if len(f.inits) > pos {
			return f.inits[pos]
		}
	}
	return f.inits[0]
}

func (f *field) Rx1(p *Params, b byte) {
	p.set(f.id, f.val.rx1(b, p.prefs))
}

func (f *field) Rx2(p *Params, a, b byte, bigEndian bool) {
	p.set(f.id, f.val.rx2(a, b, bigEndian, p.prefs))
}

func (f *field) Tx1(g interface{}) byte {
	return f.val.tx1(g, f.id)
}

func (f *field) Tx2(g interface{}, bigEndian bool) (byte, byte) {
	return f.val.tx2(g, f.id, bigEndian)
}
