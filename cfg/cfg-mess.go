package cfg

import (
	"net/url"

	"gitlab.com/jmireles/nav"
)

type MessRxFunc func(p *Params, b []byte)

type MessTxFunc func(g interface{}) []byte

// Mess is implemented by Cfg for receive and transmit real and simulated messages
type Mess interface {
	// 0. When user clicks on cfg read button we need to get the profile (if available)
	// profile value should be send as extra bytes array.
	ProfileReq(params map[string]int) []byte

	// 1. When user clicks on cfg get buttons and a simulation page is around
	// simulation is requested and this SimTx is called 1 or more times with parameters
	// by query. Returns for each request 8 bytes converted to simulate real line box responses.
	SimTx(params url.Values) ([]byte, error)

	// 2. Resp parses response 1 or more parts (8 bytes each) and writes on whole buffer
	// Here several responses comprise a whole buffer of 1 + 8*n bytes
	// The several responses should match same resp code and have marks say, 1,2,3,4
	// A complete response is send to a channel (the whole buffer)
	// When true is complete line read function is called which will call read (below).
	Resp(resp byte, part []byte, whole *[]byte) bool

	// 3. Channel release of 1 + 8*n bytes buffer is parsed here to
	// be convert to params to be used in configuration edition page
	Read(rx []byte, prefs *nav.Prefs) (*Params, error)

	// 4. Parameters in above 3. can be edited.
	// Here are converted to 1 or more parts.
	Save(params map[string]int) ([][]byte, error)

	// 5. When simulator receives saves (from 4 above), browser request
	// this server to get parameters from bytes. Here SimRx converts
	// and return parameters without i18n and fixed to imperial units.
	// This Read (see 3. above) is called.
	SimRx(line, set, resp byte, bytes [][]byte) (*Params, error)
}
