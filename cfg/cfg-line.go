package cfg

import (
	"errors"
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

type Line struct {
	cfgs []*Cfg
}

func NewLine(cfgs []*Cfg) *Line {
	return &Line{cfgs}
}

func (line *Line) Get(get byte) (*Cfg, error) {
	for _, cfg := range line.cfgs {
		if cfg != nil && cfg.comms != nil && cfg.comms.get == get {
			return cfg, nil
		}
	}
	return nil, errors.New("No cfg matches command get:" + strconv.Itoa(int(get)))
}

func (line *Line) Resp(resp byte) (*Cfg, error) {
	for _, cfg := range line.cfgs {
		if cfg != nil && cfg.comms != nil && cfg.comms.resp == resp {
			return cfg, nil
		}
	}
	return nil, errors.New("No cfg matches command resp:" + strconv.Itoa(int(resp)))
}

func (line *Line) Set(set byte) (*Cfg, error) {
	for _, cfg := range line.cfgs {
		if cfg != nil && cfg.comms != nil && cfg.comms.set == set {
			return cfg, nil
		}
	}
	return nil, errors.New("No cfg matches command set:" + strconv.Itoa(int(set)))
}

func (line *Line) SimGets() []byte {
	gets := make([]byte, 0)
	for _, cfg := range line.cfgs {
		if cfg != nil && cfg.comms != nil {
			gets = append(gets, cfg.comms.get)
		}
	}
	return gets
}

func (line *Line) SimFields() map[string][]string {
	fields := make(map[string][]string)
	for _, cfg := range line.cfgs {
		if cfg != nil && cfg.comms != nil {
			fields[strconv.Itoa(int(cfg.comms.resp))] = cfg.sim.fields
		}
	}
	return fields
}

func (line *Line) SimGetsTabs() []*nav.Tab1 {
	tabs := make([]*nav.Tab1, 0)
	for _, comm := range line.SimGets() {
		text := "Cfg" + strconv.Itoa(int(comm))
		var view func(*dom.XML, int)
		if cfg, _ := line.Get(comm); cfg == nil { // keeps no data message
			view = func(r *dom.XML, tab int) {
				r.Div(nil, `No data`)
			}
		} else {
			view = cfg.SimControls
		}
		tabs = append(tabs, nav.NewTab1Text(text, view))
	}
	return tabs
}
