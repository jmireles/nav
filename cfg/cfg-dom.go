package cfg

import (
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

type BtnFunc func(comm byte) (dom.Icon, string)

var (
	domBoxA1 = dom.A{"oncontextmenu": "return false;"}.Class("cfg-btns")
	domBoxA2 = dom.Aclass("cfg-c", "cfg-btn")
	domBoxA3 = dom.Aclass("cfg-c", "cfg-btn", "cfg-btn-confirm")

	domTabHSel   = dom.Aclass("cfg-c", "cfg-btn", "cfg-tabh-sel")
	domTabHUnsel = dom.Aclass("cfg-c", "cfg-btn", "cfg-tabh-unsel", "go")
)

func DomTabsH(xml *dom.XML, tabs []*dom.Tab) {
	xml.Div(domBoxA1, func() {
		for _, tab := range tabs {
			a := dom.A{}
			if tab.Id > 0 {
				a = domTabHUnsel.Id(tab.Id)
			} else {
				a = domTabHSel
			}
			if tab.Title != "" {
				a.Title(tab.Title)
			}
			xml.Div(a, func() {
				xml.IconText(tab.Icon, tab.Text)
			})
		}
	})
}

// calls javascript to popup editor
func domAttrsHelp(req *nav.JsonReq, g, f int) dom.A {
	post := nav.JsonPostHelp(req, g, f)
	return dom.Aclass(`cfg-field`).Onclick(post.JavascriptOnclick())
}

func domAttrsOptionEditor(gs, fs string, number int, param *Param) dom.A {
	attrs := dom.A{
		"oncontextmenu": `return false;`, // prevent mouse right button click operation
	}
	if number == param.number {
		attrs = attrs.Class(`cfg-opt`)
	} else {
		attrs = attrs.Class(`cfg-opt`, `cfg-opt-sel`)
		attrs = attrs.Onclick(`cfg.updateParam(` +
			gs +
			`,` + fs +
			`,` + strconv.Itoa(param.number) +
			`,\"` + param.scaled + `\"` +
			`)`)
	}
	return attrs
}

func domAttrsEditorNumber(post *nav.JsonPost, g, f, number int) dom.A {
	// class lets the param mouse hover informs user to click to edit
	// name Helps in DOM find all parameters to get data to be saved.
	// id Identifies the group-field among others to update DOM after edition
	// calls javascript to popup editor
	return dom.A{"data-int": strconv.Itoa(number)}.
		Name("cfg-param").
		Class(`cfg-value`).
		Id(`cfg-param-` + strconv.Itoa(g) + "-" + strconv.Itoa(f)).
		Onclick(post.JavascriptOnclick())
}

func domAttrsUnits(title string) dom.A {
	return dom.Aclass(`cfg-units`).Title(title)
}

func DomHelp(xml *dom.XML, req *nav.JsonReq, f int, text string) {
	attrs := domAttrsHelp(req, 0, f)
	xml.Div(attrs, text)
}

func DomEditorNumber(xml *dom.XML, post *nav.JsonPost, g, f int, number int) {
	attrs := domAttrsEditorNumber(post, g, f, number)
	xml.Div(attrs, strconv.Itoa(number))
}

// DomButtonShowNoResp writes into xml a clickable button a "show" icon and a given text.
// The button action does not expect a response, so no DOM is returned to receive such response.
func DomButtonShowNoResp(xml *dom.XML, post *nav.JsonPost, text string) {
	xml.Div(domBoxA3.Onclick(post.JavascriptOnclick()), func() {
		xml.IconText(IconShow, text)
	})
}

// DomButtonGetResp writes into xml a clickable button with a "get" icon and a given text.
// The button action expects a response, so a DOM with id respId is added where response is received.
func DomButtonGetResp(xml *dom.XML, post *nav.JsonPost, name string, respId int) {
	xml.Div(domBoxA1, func() {
		xml.Div(domBoxA3.Onclick(post.JavascriptOnclick()), func() {
			xml.IconText(IconGet, name)
		})
		xml.Div(domBoxA2, func() {
			xml.Svg(dom.Aclass("icon0"), func() {}) // 0-width no icon lets next text aligns
			xml.Span(dom.Aid(respId), "")           // response id
		})
	})
}

func DomBoxesReqResp(xml *dom.XML, title string, post *nav.JsonPost, respId int, btnFunc BtnFunc) {
	xml.Div(domBoxA1, func() {
		xml.Div(domBoxA2, func() {
			xml.IconText(IconBox, title)
		})
		icon, name := btnFunc(0)
		xml.Div(domBoxA3.Onclick(post.JavascriptOnclick()), func() {
			xml.IconText(icon, name)
		})
		xml.Div(domBoxA2, func() {
			xml.Svg(dom.Aclass("icon0"), func() {}) // 0-width no icon lets next text aligns
			xml.Span(dom.Aid(respId), "")           // response id
		})
	})
}

// DomBoxAndResp draws a DOM row with box number and cfg buttons as Tabs.
// Below the row draws a DOM div with id "cfg-resp" where all responses
// will be set with innerHTML.
func DomBoxReqResp(xml *dom.XML, nets []string, net, line byte, ids [][]int, box byte, btnFunc BtnFunc) {
	key := "cfg-resp"
	xml.Div(domBoxA1, func() {
		// box-icon (tag) and box-number
		xml.Div(domBoxA2, func() {
			if len(nets) > int(net) {
				xml.IconText(IconNet, nets[net])
			}
			xml.IconText(IconBox, strconv.Itoa(int(box)))
		})
		for _, c := range ids {
			command := byte(c[0])
			// value is innerHTML code set at dom with id key after onclick
			// should have double quotes
			value := `<div id=\"` + strconv.Itoa(c[1]) + `\"></div>`
			post := nav.JsonPostGet(net, line, command, box, key, value)
			icon, name := btnFunc(command)
			xml.Div(domBoxA3.Onclick(post.JavascriptOnclick()).Title(name), func() {
				xml.Icon(icon)
			})
		}
	})
	// This dom with id equals key will be filled with innerHTML
	xml.Div(dom.Aid(key).Class("cfg"), nil)
}

func domAttrsGroupTab(g int) dom.A {
	if g == 0 {
		return nil
	}
	// g > 0 groups with tabbed parameters (not profiles)
	var gClass string
	if g == 1 {
		gClass = "cfg-sel"
	} else {
		gClass = "cfg-unsel"
	}
	gs := strconv.Itoa(g)
	return dom.Aclass(gClass).
		Name("cfg-gb").
		Id(`cfg-gb-` + gs).
		Onclick(`cfg.showG(` + gs + `)`)
}

func domAttrsGroupTable(g int) dom.A {
	if g == 0 {
		return nil
	}
	// g > 0 groups with tabbed parameters (not profiles)
	var gClass string
	if g == 1 {
		gClass = "shown"
	} else {
		gClass = "hidden"
	}
	gs := strconv.Itoa(g)
	return dom.Aclass(gClass).Name(`cfg-g`).Id(`cfg-g-` + gs)
}

type domBtn struct {
	icon    dom.Icon
	text    string
	style   int
	onclick string
	title   string
}

const (
	domStyleNone = iota
	domStyleCancel
	domStyleConfirm
	domStyleRead
	domStyleSaveBox
	domStyleSaveAll
)

var (
	domAttrsCenter = dom.Aclass("cfg-center")
	domAttrsEditor = dom.Aclass("cfg-flex", "invisible").Id("cfg-editor")
	domAttrsErr    = dom.Aclass("cfg-err")
	domAttrsFlex   = dom.Aclass("cfg-flex")
	domAttrsGet    = dom.Aclass("cfg-get")
	domAttrsGroup  = dom.Aclass("cfg-c", "cfg-resp")
	domAttrsNil    = dom.Aclass("cfg-nil")
	domAttrsNumber = dom.Aclass("cfg-c", "cfg-number")
)

func domBtns(xml *dom.XML, buttons []*domBtn) {
	for _, b := range buttons {
		domBtnW(xml, b)
	}
}

func domBtnsSaveBoxAll(post *nav.ReqPost) {
	boxI18n, boxOnclick := post.SaveBoxOnclick()
	allI18n, allOnclick := post.SaveAllOnclick()
	domBtnsSave(post.XML(), []*domBtn{
		nil,
		&domBtn{IconSave, boxI18n, domStyleSaveBox, boxOnclick, ""},
		&domBtn{IconSave, allI18n, domStyleSaveAll, allOnclick, ""},
	})
}

func domBtnsSave(xml *dom.XML, buttons []*domBtn) {
	xml.Div(domAttrsEditor, func() {
		domBtns(xml, buttons)
	})
}

func domBtnsFlex(xml *dom.XML, inner func()) {
	xml.Div(domAttrsFlex, inner)
}

func domBtnW(x *dom.XML, b *domBtn) {
	if b == nil {
		// call func in order to get
		// <div ...></div> instead <div .../>
		// to work correctly inside flex
		x.Div(domAttrsNil, func() {

		})
		return
	}
	var class string
	switch b.style {
	case domStyleCancel:
		class = `cfg-btn-cancel`
	case domStyleConfirm:
		class = `cfg-btn-confirm`
	case domStyleRead:
		class = `cfg-btn-read`
	case domStyleSaveBox:
		class = `cfg-btn-save-box`
	case domStyleSaveAll:
		class = `cfg-btn-save-all`
	}
	attrs := dom.A{"oncontextmenu": "return false;"}.
		Class("cfg-c", "cfg-btn", class)
	if b.onclick != "" {
		attrs = attrs.Onclick(b.onclick)
	}
	if b.title != "" {
		attrs = attrs.Title(b.title)
	}
	x.Div(attrs, func() {
		x.IconText(b.icon, b.text)
	})
}

func domError(x *dom.XML, text string) {
	x.Div(domAttrsErr, text)
}

func domGet(x *dom.XML, text string) {
	x.Div(domAttrsGet, text)
}

func domGroup(x *dom.XML, b *domBtn) {
	x.Div(domAttrsGroup, func() {
		x.IconText(b.icon, b.text)
	})
}
