package cfg

import (
	"net/url"
	"strconv"

	"gitlab.com/jmireles/nav"
)

type Scale struct {
	Factor   float32
	Base     int
	Decimals int
}

// Return this scale and the string value scaled properly
// The calculation used equals factor * (val - base) with decimals.
// With this we get the value ready to be presented in a DOM
// and the scale factor, base and decimals to be used by a
// javascript method to scale values changed in a web page
func (s *Scale) scaling(val int) (*Scale, string) {
	f := s.Factor * float32(val-s.Base)
	return s, strconv.FormatFloat(float64(f), 'f', s.Decimals, 32)
}

func (s *Scale) scalingReverse(val string) (i int) {
	if f, err := strconv.ParseFloat(val, 32); err == nil {
		i = int(float32(s.Base) + 0.5 + float32(f)/s.Factor)
	} else {
		i = 0
	}
	return
}

type Scales struct {
	Metric   *Scale
	Imperial *Scale
}

type val interface {
	unitLabel(*nav.Prefs) *Label
	tx1(g interface{}, f string) byte
	rx1(value byte, prefs *nav.Prefs) *Param
	tx2(g interface{}, f string, bigEndian bool) (byte, byte)
	rx2(a, b byte, bigEndian bool, prefs *nav.Prefs) *Param
}

type value struct {
	unit   *I18nUnit
	scales *Scales
}

func (v *value) unitLabel(prefs *nav.Prefs) *Label {
	if v.unit == nil {
		return nil
	}
	return v.unit.Get(prefs)
}

// options has several keys to choose from an option
// values convert option to a value to be stored as numeric
// i18n lets to translate the keys
type options struct {
	keys   []string
	values []byte
	mask   byte
	i18n   *I18nOpt
	value
}

func Options(keys []string, values []byte, mask byte, i18n *I18nOpt, unit *I18nUnit) *options {
	if len(keys) != len(values) {
		panic("keys size different values size")
	}
	return &options{keys, values, mask, i18n, value{unit, nil}}
}

// Returns a byte representation of
func (o *options) tx1(g interface{}, f string) byte {
	switch g.(type) {
	case map[string]int:
		// integer value from DOM edited values
		// are returned to be saved in CANs
		value := byte(g.(map[string]int)[f])
		for _, v := range o.values {
			if v == value {
				return v & o.mask
			}
		}
	case url.Values:
		// simulated values in web pages
		// are returned to viewer cfg response
		key := g.(url.Values).Get(f)
		for i, k := range o.keys {
			if k == key {
				return o.values[i] & o.mask
			}
		}
	}
	return byte(0)
}

func (o *options) tx2(g interface{}, f string, bigEndian bool) (byte, byte) {
	// should not be used
	if bigEndian {
		return 0, o.tx1(g, f)
	} else {
		return o.tx1(g, f), 0
	}
}

func (o *options) rx2(a, b byte, bigEndian bool, prefs *nav.Prefs) *Param {
	// should not be used
	if bigEndian {
		return o.rx1(a, prefs)
	} else {
		return o.rx1(b, prefs)
	}
}

// Returns a string translated representation of value
func (o *options) rx1(value byte, prefs *nav.Prefs) *Param {
	valueM := value & o.mask
	for pos, v := range o.values {
		if v == valueM {
			key := o.keys[pos]
			scaled := key
			if prefs != nil && o.i18n != nil {
				i18n := o.i18n.Get(key)
				scaled = i18n.Get(prefs.Lang)
			} // for simulation (prefs==nil) or no i18n, scaled is the key
			number := int(valueM)
			return &Param{scaled, number}
		}
	}
	// on error scaled is empty
	return &Param{"", int(valueM)}
}

func (o *options) Write(value byte) string {
	for i, v := range o.values {
		if v == value {
			return o.keys[i]
		}
	}
	return ""
}

func (o *options) editorParams(prefs *nav.Prefs) []*Param {
	params := make([]*Param, len(o.keys))
	for pos, key := range o.keys {
		scaled := key
		if o.i18n != nil {
			i18n := o.i18n.Get(key)
			scaled = i18n.Get(prefs.Lang)
		} // for no i18n, scaled is the key
		number := int(o.values[pos] & o.mask)
		params[pos] = &Param{scaled, number}
	}
	return params
}

type number struct {
	min int
	max int
	value
}

func Number(min, max int, unit *I18nUnit, scales *Scales) *number {
	if min > max {
		panic("min greater than max")
	}
	return &number{min, max, value{unit, scales}}
}

func (n *number) scaling(prefs *nav.Prefs, val int) (*Scale, string) {
	if val < n.min {
		val = n.min
	}
	if val > n.max {
		val = n.max
	}
	if n.scales != nil {
		if prefs == nil {
			// for simulation try first imperial, then metric
			if n.scales.Imperial != nil {
				return n.scales.Imperial.scaling(val)
			} else if n.scales.Metric != nil {
				return n.scales.Metric.scaling(val)
			}
		} else {
			switch prefs.Units {
			case nav.UnitsMetric:
				return n.scales.Metric.scaling(val)
			case nav.UnitsImperial:
				return n.scales.Imperial.scaling(val)
			}
		}
	}
	return nil, strconv.Itoa(val)
}

// Used by simulator to convert from possible float to integer
// Simulator always use Imperial units so no prefs are needed
// to be read here
func (n *number) scalingReverse(val string) int {
	if n.scales != nil {
		if n.scales.Imperial != nil {
			return n.scales.Imperial.scalingReverse(val)
		} else if n.scales.Metric != nil {
			return n.scales.Metric.scalingReverse(val)
		}
	} else if i, err := strconv.Atoi(val); err == nil {
		return i
	}
	return 0
}

// Return a byte representation of string value
// value is generated in a simulation.
func (n *number) tx1(g interface{}, f string) byte {
	switch g.(type) {
	case map[string]int:
		// integer value from DOM edited values
		// are returned to be saved in CANs
		return byte(g.(map[string]int)[f])
	case url.Values:
		// simulated values in web pages
		// are returned to viewer cfg response
		value := g.(url.Values).Get(f)
		if i, err := strconv.Atoi(value); err != nil {
			return byte(0)
		} else {
			return byte(i)
		}
	}
	return byte(0)
}

// Return a string representation of byte value
// value comes as byte in a protocol group of bytes
func (n *number) rx1(value byte, prefs *nav.Prefs) *Param {
	// TODO if scale use prefs to use metric
	number := int(value)
	_, scaled := n.scaling(prefs, number)
	return &Param{scaled, number}
}

func (n *number) tx2(g interface{}, f string, bigEndian bool) (byte, byte) {
	i2 := 0
	switch g.(type) {
	case map[string]int:
		// integer value from DOM edited values
		// are returned to be saved in CANs
		if n, ok := g.(map[string]int)[f]; ok {
			i2 = n
		}
	case url.Values:
		// simulated values in web pages can be float
		// are returned to viewer cfg response
		value := g.(url.Values).Get(f)
		i2 = n.scalingReverse(value)
	}
	if i2 < 0 {
		i2 = 1
	} else if i2 > 0xffff {
		i2 = 0xffff
	}
	if bigEndian {
		return byte(i2 >> 8), byte(i2)
	} else {
		return byte(i2), byte(i2 >> 8)
	}
}

func (n *number) rx2(a, b byte, bigEndian bool, prefs *nav.Prefs) *Param {
	number := 0
	if bigEndian {
		number = int(a)<<8 + int(b)
	} else {
		number = int(b)<<8 + int(a)
	}
	_, scaled := n.scaling(prefs, number)
	return &Param{scaled, number}
}
