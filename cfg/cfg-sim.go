package cfg

import (
	"strconv"
	"strings"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

func SimHelp(xml *dom.XML, elem, title, text string) {
	xml.W0(`<` + elem + ` class="title small" title="` + title + `">` + text + `</` + elem + `>`)
}

type sim struct {
	fields []string
	cfg    *Cfg
	f0     Fields
	idResp string
	idSet  string
}

func newSim(fields []Fields, cfg *Cfg) *sim {
	// fields names arranged in groups for simulator to know what fields ids
	// to send group by group
	s := make([]string, len(fields))
	for i := 0; i < len(fields); i++ {
		a := make([]string, len(fields[i]))
		for p, s := range fields[i] {
			a[p] = s.id
		}
		s[i] = strings.Join(a, ",")
	}
	g0 := cfg.gfms.groups[0]
	return &sim{
		fields: s,
		cfg:    cfg,
		f0:     cfg.gfms.fields[g0], // profiles
		idResp: cfg.comms.lineS + `-` + strconv.Itoa(int(cfg.comms.resp)),
		idSet:  cfg.comms.lineS + `-` + strconv.Itoa(int(cfg.comms.set)),
	}
}

func (s *sim) controls(xml *dom.XML) {
	c := s.cfg
	comm := c.i18n.Id.Get(nav.LangEn)
	xml.Div(nil, `<b>`+comm+`</b>`)
	if len(s.f0) == 0 {
		s.controls2(xml, 0)
	} else {
		profile := s.f0[0]
		switch profile.val.(type) {
		case *options:
			o := profile.val.(*options)
			for pos, opt := range o.keys {
				xml.W("hr", nil, nil)
				xml.Div(nil, `Profile `+opt)
				s.controls2(xml, pos+1)
			}
		}
	}
}

func (s *sim) controls2(xml *dom.XML, profile int) {
	idResp := s.idResp
	idSet := s.idSet
	c := s.cfg
	if profile > 0 {
		profileS := strconv.Itoa(profile)
		// see ../p21/go/cans/sim/web/templates/sim-cfg.js resp where -prof is checked
		// TODO: use template variable?
		idResp += "-prof" + profileS
		idSet += "-prof" + profileS
	}
	xml.W0(`<dl><dt>Read</dt><dd>`)
	for g, group := range c.gfms.groups {
		if g == 0 && profile == 0 {
			continue
		}
		xml.W0(`<div class="group">`)
		xml.W0(`<table>`)
		xml.W0(`<caption title='group'><i>` + group + `</i></caption>`)
		for f, field := range c.gfms.fields[group] {
			id := idResp + "-" + field.id // Example: p1-3-pAct
			xml.Tr(func() {
				help := c.i18nField(group, field, nil)
				SimHelp(xml, "td", help, field.id)
				xml.Td(nil, func() {
					switch field.val.(type) {
					case *options:
						s.opts(xml, id, field, g, f, profile)
					case *number:
						s.number(xml, id, field, g, f, profile)
					}
				})
				if unit := field.val.unitLabel(nil); unit != nil {
					xml.Td(nil, `<i>`+unit.Abbrev+`</i>`)
				}
			})
		}
		xml.W0(`</table></div>`)
	}
	xml.W0(`</dd><dt>Save</dt><dd>`)
	xml.W0(`<div id="` + idSet + `-save"></div>`)
	xml.W0(`</dd></dl>`)
}

func (s *sim) opts(xml *dom.XML, id string, field *field, g, f int, profile int) {
	o := field.val.(*options)
	xml.W0(`<select`)
	xml.W0(` id="` + id + `" class='original' title="id=` + id + `">`)
	if g == 0 && f == 0 {
		// profile opts should have a single option fixed
		// will be used as value to be responded by reads
		for pos, opt := range o.keys {
			if profile == pos+1 {
				sel := " selected"
				xml.W0(`<option value="` + opt + `"` + sel + `>` + opt + `</option>`)
			}
		}
	} else {
		// non-profile opts is normal having several options to
		// choose from to be returned to reads requests
		// init or default value is selected for each profile
		// as was set in field creation
		init := field.init(profile)
		for _, opt := range o.keys {
			sel := ""
			if init == opt {
				sel = " selected"
			}
			xml.W0(`<option value="` + opt + `"` + sel + `>` + opt + `</option>`)
		}
	}
	xml.W0(`<select>`)
}

func (s *sim) number(xml *dom.XML, id string, field *field, g, f int, profile int) {
	n := field.val.(*number)
	_, min := n.scaling(nil, n.min)
	_, max := n.scaling(nil, n.max)
	step := "1"
	if n.scales != nil {
		dec := 0
		if n.scales.Imperial != nil {
			dec = n.scales.Imperial.Decimals
		} else if n.scales.Metric != nil {
			dec = n.scales.Metric.Decimals
		}
		switch dec {
		case 1:
			step = "0.1"
		case 2:
			step = "0.01"
		default:
			step = "0.001"
		}
	}
	// init default numeric value should be different for each profile
	// as were set at field creation.
	init := field.init(profile)
	attrs := dom.AtypeNumber(min, max, init, step).
		Id(id).
		Class("original").
		Title(`id=` + id + `, min=` + min + `, init=` + init + `, max=` + max)

	xml.Input(attrs)
}
