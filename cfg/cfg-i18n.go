package cfg

import (
	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

var (
	IconBox    dom.Icon
	IconCancel dom.Icon
	IconDown   dom.Icon
	IconEdit   dom.Icon
	IconHelp   dom.Icon
	IconGet    dom.Icon
	IconNet    dom.Icon
	IconSave   dom.Icon
	IconShow   dom.Icon
	IconUp     dom.Icon
)

type I18n struct {
	Id     nav.I18Ns            `yaml:"i"`
	Groups map[string]I18nGroup `yaml:"g"`
}

type I18nGroup struct {
	Id     nav.I18Ns            `yaml:"i"`
	Fields map[string]I18nField `yaml:"f"`
}

type I18nField struct {
	Id   nav.I18Ns      `yaml:"i"`
	Help nav.I18NsArray `yaml:"h"`
}

type I18nOpt map[string]nav.I18Ns

func (o *I18nOpt) Get(k string) nav.I18Ns {
	if i, ok := (*o)[k]; ok {
		return i
	}
	return nav.I18Ns{} // empty
}

type I18nOpts map[string]I18nOpt

func (o *I18nOpts) Get(k string) *I18nOpt {
	if i, ok := (*o)[k]; ok {
		return &i
	}
	return &I18nOpt{}
}

type I18nUnits map[string]*I18nUnit

func (u *I18nUnits) Get(k string) *I18nUnit {
	if i, ok := (*u)[k]; ok {
		return i
	}
	return &I18nUnit{} // empty
}

type I18nUnit struct {
	Abbrev nav.I18Ns `yaml:"a"` // default imperail abbreviation
	Title  nav.I18Ns `yaml:"t"` // default imperial description
	Metric struct {
		Abbrev nav.I18Ns `yaml:"a"` // metric optional abbreviation
		Title  nav.I18Ns `yaml:"t"` // metric optional description
	} `yaml:"m"` // imperial
}

// Return abbreviation and title of units
func (u *I18nUnit) Get(prefs *nav.Prefs) *Label {
	var a, t string
	if prefs != nil {
		if prefs.Units == nav.UnitsMetric {
			a = u.Metric.Abbrev.Get(prefs.Lang)
			t = u.Metric.Title.Get(prefs.Lang)
		}
		if a == "" || t == "" {
			a = u.Abbrev.Get(prefs.Lang)
			t = u.Title.Get(prefs.Lang)
		}
	} else {
		a = u.Abbrev.Get("en")
		t = u.Title.Get("en")
	}
	return &Label{a, t}
}
