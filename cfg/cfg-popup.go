package cfg

import (
	"strconv"

	"gitlab.com/jmireles/nav"
	"gitlab.com/jmireles/nav/dom"
)

type popup struct {
	post  *nav.ReqPost
	icon  dom.Icon
	title string
}

func (p *popup) resp(footer func()) *nav.JsonResp {
	xml := p.post.XML()
	xml.Div(dom.Aclass("cfg-c"), func() {
		xml.IconText(p.icon, p.title)
	})
	xml.W("hr", nil, nil)
	footer()
	return nav.NewJsonRespPopup(xml.String(), nil)
}

func (p *popup) btnsCenter(buttons []*domBtn) {
	xml := p.post.XML()
	xml.Div(domAttrsCenter, func() {
		domBtns(xml, buttons)
	})
}

func (p *popup) btnCancel() *domBtn {
	onclick := `popup.close()`
	return &domBtn{"", p.post.I18nCancel(), domStyleCancel, onclick, ""}
}

func (p *popup) btnSaveParams(values string) *domBtn {
	onclick := "cfg.saveParams(" + values + ")"
	return &domBtn{"", p.post.I18nSave(), domStyleConfirm, onclick, ""}
}

func (p *popup) btnSaveParam(values, gs, fs string) *domBtn {
	onclick := `cfg.saveParam(` + values + `,` + gs + `,` + fs + `)`
	return &domBtn{"", p.post.I18nSave(), domStyleConfirm, onclick, ""}
}

func (p *popup) btnUpdateParam(gs, fs string) *domBtn {
	onclick := `cfg.updateParamNumber(` + gs + `,` + fs + `)`
	return &domBtn{"", p.post.I18nConfirm(), domStyleConfirm, onclick, ""}
}

// use this button with ws.js cfg.changeNumber which internally
// pass this DOM to append onmouseup. Both mouse down/up can
// manage to increment/decrement one by one or keep it pressed
// to auto-change first slow then fast.
func (p *popup) btnNumberChange(icon dom.Icon, set int, gs, fs, up string) {
	class := `cfg-btn-get`
	if set == 1 {
		class = `cfg-btn-set`
	} else if set == 2 {
		class = `cfg-btn-cancel`
	}
	attrs := dom.A{
		"oncontextmenu": `return false;`,
		"onmousedown":   `cfg.changeNumber(this,` + gs + `,` + fs + `,` + up + `);'`,
	}.Class(`cfg-c`, class)
	xml := p.post.XML()
	xml.Div(attrs, func() {
		xml.IconText(icon, "")
	})
}

func (p *popup) valNumberEdition(gs, fs string, val int, n *number) {
	xml := p.post.XML()
	prefs := p.post.Prefs()
	scale, scaled := n.scaling(prefs, val)
	xml.Div(domAttrsNumber, func() {
		attrs := dom.A{
			"oncontextmenu": `return false;`,
			"data-min":      strconv.Itoa(n.min),
			"data-val":      strconv.Itoa(val),
			"data-max":      strconv.Itoa(n.max),
		}.
			Id(`cfg-editor-` + gs + `-` + fs).
			Style(`width:40px`)
		if scale != nil {
			attrs["data-factor"] = strconv.FormatFloat(float64(scale.Factor), 'f', 5, 32)
			attrs["data-base"] = strconv.Itoa(scale.Base)
			attrs["data-decimals"] = strconv.Itoa(scale.Decimals)
		}
		xml.Div(attrs, scaled)
	})
}

func PopupHelp(post *nav.ReqPost, header string, paragraphs []string) *nav.JsonResp {
	p := &popup{post, IconHelp, header}
	return p.resp(func() {
		for _, paragraph := range paragraphs {
			post.XML().W("p", nil, paragraph)
		}
	})
}

func PopupEdit(post *nav.ReqPost, title, gs, fs string, v interface{}) *nav.JsonResp {
	p := &popup{post, IconEdit, title}
	return p.resp(func() {
		xml := post.XML()
		valInt, ok := post.DomInt()
		if !ok {
			domError(xml, "Dom.Id not number")
			return
		}
		switch v.(type) {
		case *options:
			o := v.(*options)
			xml.W("table", domAttrsCenter, func() {
				for _, param := range o.editorParams(post.Prefs()) {
					xml.Tr(func() {
						attrs := domAttrsOptionEditor(gs, fs, valInt, param)
						xml.Td(attrs, param.scaled)
					})
				}
			})
			xml.W("hr", nil, nil)
			p.btnsCenter([]*domBtn{
				p.btnCancel(),
			})
		case *number:
			n := v.(*number)
			xml.Div(domAttrsCenter, func() {
				p.btnNumberChange(IconDown, domStyleConfirm, gs, fs, "0")
				p.valNumberEdition(gs, fs, valInt, n)
				p.btnNumberChange(IconUp, domStyleConfirm, gs, fs, "1")
			})
			xml.W("hr", nil, nil)
			if post.Req().Serial == nil {
				p.btnsCenter([]*domBtn{
					p.btnUpdateParam(gs, fs),
					p.btnCancel(),
				})
			} else {
				_, values := post.SaveTitleValues()
				p.btnsCenter([]*domBtn{
					p.btnSaveParam(values, gs, fs),
					p.btnCancel(),
				})
			}
		default:
			domError(xml, "v not options/number")
		}
	})
}

func PopupSave(post *nav.ReqPost) *nav.JsonResp {
	title, values := post.SaveTitleValues()
	p := &popup{post, IconSave, title}
	return p.resp(func() {
		p.btnsCenter([]*domBtn{
			p.btnSaveParams(values),
			p.btnCancel(),
		})
	})
}
