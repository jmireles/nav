package nav

import (
	"strconv"

	"gitlab.com/jmireles/nav/dom"
)

type Tab struct {
	icon  dom.Icon
	text  string
	title string
}

type Tab1 struct {
	*Tab
	view func(*dom.XML, int)
}

func NewTab1Text(text string, view func(*dom.XML, int)) *Tab1 {
	return &Tab1{Tab: &Tab{text: text}, view: view}
}

func NewTab1IconText(icon dom.Icon, text string, view func(*dom.XML, int)) *Tab1 {
	return &Tab1{Tab: &Tab{icon: icon, text: text}, view: view}
}

func NewTab1IconTitle(icon dom.Icon, title string, view func(*dom.XML, int)) *Tab1 {
	return &Tab1{Tab: &Tab{icon: icon, title: title}, view: view}
}

type Tabs1 []*Tab1

func (tabs Tabs1) Write(r *dom.XML, key string, viewClass string) {
	r.Div(dom.Aclass(key+"s"), func() {
		for t, tab := range tabs {
			c := dom.A{}
			if t == 0 {
				c.Class(key + `-tab-sel`)
			} else {
				c.Class(key + `-tab-unsel`)
			}
			p := strconv.Itoa(t)
			c.Id(key + `-` + p)
			c.Onclick(`tabs.show("` + key + `",` + p + `)`)
			c.Name(key)

			if tab.title != "" {
				c.Title(tab.title)
			}
			r.Div(c, func() {
				if tab.icon != "" {
					r.Icon(tab.icon)
				}
				if tab.text != "" {
					r.Span(nil, tab.text)
				}
			})
		}
	})
	c := dom.A{}
	if viewClass != "" {
		c.Class(viewClass)
	}
	r.Div(c, func() {
		for t, tab := range tabs {
			c := dom.A{}
			if t > 0 {
				c.Class(`hidden`)
			} else {
				c.Class(`show`)
			}
			c.Id(key + `-tab-` + strconv.Itoa(t))
			c.Name(key + `-tab`)
			r.Div(c, func() {
				tab.view(r, t)
			})
		}
	})
}

func TabsTemplatesReadJS(xml *dom.XML) {
	templates.Read(xml, "tabs.js", nil)
}

func TabCssVerticalHorizontal(xml *dom.XML, vId string, hIds []string) {
	xml.W0(`
.` + vId + `s {
	position: fixed;
	top: 0px;
	left: 0px;
	padding: 0px;
	color: #000;
	width: 30px;
}
.` + vId + `s > div {
	height: 30px;
	width: 30px;
	display: block;
	margin: 0px 30px 0px 0px;
	padding: 0px;
}
.` + vId + `-tab-sel {
	fill: #f40;
	border-right: 4px solid #f40;
}
.` + vId + `-tab-unsel {
	fill: #aaa;
	border-right: 4px solid transparent;
}
.` + vId + `-tab-unsel:hover {
	border-right: 4px solid #aaa;
	cursor: pointer;
}
.` + vId + `-tab-content {
	margin: 0 0 0 40px;
}`)
	for _, h := range hIds {
		xml.W0(`
.` + h + `s {
	user-select: none;
	display: inline-block;
	margin: 0 0 10px 40px;
	color: #000;
}
.` + h + `s > div {
	display: inline-block;
	margin:4px;
	padding:0px;
}
.` + h + `-tab-sel {
	fill: #000;
	color: #000;
	border-bottom: 4px solid #f00;
}
.` + h + `-tab-unsel {
	fill: #888;
	color: #888;
	border-bottom: 4px solid transparent;
}
.` + h + `-tab-unsel:hover {
	cursor: pointer;
	border-bottom: 4px solid #888;
}
.` + h + `-tab-content {
	margin: 0 0 0 40px;
	user-select: none;
}
.` + h + `-tab-sel > span, .` + h + `-tab-unsel > span {
	vertical-align: top;
	line-height: 35px;
	font-size:16px;
	padding: 0 6px 0 0;
}
`)
	}
}
