package ws

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type Attrs map[string]string // see nav/dom/attr.go

func (t Attrs) check(s string) (string, bool) {
	if t == nil {
		return "", false
	}
	return t[s], true
}

func (t Attrs) Fill() (string, bool) {
	return t.check("f")
}

func (t Attrs) Stroke() (string, bool) {
	return t.check("s")
}

func (t Attrs) Text() (string, bool) {
	return t.check("t")
}

type IAttrs struct {
	Attrs map[int]Attrs `json:"attrs"`
}

func NewIAttrs(message []byte) (*IAttrs, error) {
	var attrs *IAttrs
	if err := json.Unmarshal(message, &attrs); err != nil {
		return nil, err
	} else if attrs == nil {
		return nil, errors.New("empty attrs")
	} else {
		return attrs, nil
	}
}

func (t *IAttrs) Get(id int) Attrs {
	return t.Attrs[id]
}

func (t *IAttrs) Text(id int) (string, error) {
	if m := t.Get(id); m == nil {
		return "", errors.New("not found")
	} else if text, ok := m.Text(); !ok {
		return "", errors.New("not found")
	} else {
		return text, nil
	}
}

func TestBrowser(client *Client, stop chan int, respF func([]byte, error)) {
	if client == nil {
		respF(nil, errors.New("test browser client is nil"))
	}
	s := httptest.NewServer(client)
	wsURL, err := url.Parse(s.URL)
	if err != nil {
		respF(nil, err)
		return
	}
	switch wsURL.Scheme {
	case "http":
		wsURL.Scheme = "ws"
	case "https":
		wsURL.Scheme = "wss"
	}
	ws, _, err := websocket.DefaultDialer.Dial(wsURL.String(), nil)
	if err != nil {
		respF(nil, err)
		return
	}
	normal := websocket.CloseNormalClosure
	go func() {
		s := <-stop
		if s == 0 {
			// caller sent stop=0, cleanly close the connection
			ws.WriteMessage(websocket.CloseMessage,
				websocket.FormatCloseMessage(normal, ""))
		} else {

		}
	}()
	go func() {
		defer s.Close()
		defer ws.Close()
		// keep reading messages from server responses updates
		for {
			if _, m, err := ws.ReadMessage(); err != nil {
				if websocket.IsCloseError(err, normal) {
					return
				} else {
					respF(nil, err)
				}
			} else {
				respF(m, nil)
			}
		}
	}()
}

type TestWsServer struct {
	off chan bool
}

func (w *TestWsServer) Off(b bool) {
	w.off <- b
}

func NewTestWsServer(client http.Handler, stop chan error, respF func(*IAttrs)) *TestWsServer {
	tws := &TestWsServer{
		off: make(chan bool, 1),
	}
	s := httptest.NewServer(client)
	wsURL, err := url.Parse(s.URL)
	if err != nil {
		stop <- err
		return nil
	}
	switch wsURL.Scheme {
	case "http":
		wsURL.Scheme = "ws"
	case "https":
		wsURL.Scheme = "wss"
	}
	ws, _, err := websocket.DefaultDialer.Dial(wsURL.String(), nil)
	if err != nil {
		stop <- err
		return nil
	}
	normal := websocket.CloseNormalClosure
	go func() {
		<-tws.off
		ws.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(normal, ""))
	}()
	go func() {
		defer s.Close()
		defer ws.Close()
		// keep reading messages from server responses updates
		for {
			if _, m, err := ws.ReadMessage(); err != nil {
				if websocket.IsCloseError(err, normal) {
					return
				} else {
					stop <- err
				}
			} else {
				var attrs *IAttrs
				if err := json.Unmarshal(m, &attrs); err != nil {
					stop <- err
				} else if attrs != nil {
					respF(attrs)
				}
			}
		}
	}()
	return tws
}

type WaitGroup struct {
	sync.WaitGroup
}

func (wg *WaitGroup) WaitTimeout(timeout time.Duration) bool {
	done := make(chan struct{})
	go func() {
		defer close(done)
		wg.Wait()
	}()
	select {
	case <-done:
		return false
	case <-time.After(timeout):
		return true
	}
}
