package ws

import (
	"net/http"
	"testing"

	"gitlab.com/jmireles/nav/dom"
)

// https://github.com/fteem/go-playground/tree/master/testing-in-go-web-sockets

// Basic viewer builds and adds iattrs, returns viewer
// and does nothing with DOM function
type v struct {
	view *View
}

func newV(view *View, iAttrs dom.IAttrs) *v {
	if iAttrs != nil {
		view.Push(&dom.Push{IAttrs:iAttrs, Section:dom.Section(0)})
	}
	return &v{view}
}

// implements ws.Viewer
func (v v) View() *View {
	return v.view
}

// implements ws.Viewer
func (v v) DOM(click *dom.Click, x *dom.XML) (dom.Section, error) {
	return 0, nil
}

// Viewer1 does not filter any attrs, any id passes
type v1 struct {
	*v
}

func newV1(view *View, iAttrs dom.IAttrs) v1 {
	return v1{newV(view, iAttrs)}
}

// implements ws.Viewer
func (v v1) AttrsFilter(click *dom.Click, section dom.Section) PageFilterFunc {
	return func(id int, attrs *dom.Attrs) *dom.Attrs {
		return attrs // pass everything
	}
}

// View2 filters ids odd numbers and passes even numbers
// As a filter is a "halfer" for continuous range of consecutive ids
type v2 struct {
	*v
}

func newV2(view *View, iAttrs dom.IAttrs) v2 {
	return v2{newV(view, iAttrs)}
}

// implements ws.Viewer
func (v v2) AttrsFilter(click *dom.Click, section dom.Section) PageFilterFunc {
	return func(id int, attrs *dom.Attrs) *dom.Attrs {
		if id%2 == 0 {
			return attrs // pass even ids
		}
		return nil // filter odd ids
	}
}

type control struct {
	t    *testing.T
	stop chan error
	wss  []*TestWsServer
}

func newControl(t *testing.T) *control {
	return &control{
		t:    t,
		stop: make(chan error, 1),
		wss:  make([]*TestWsServer, 0),
	}
}

func (c *control) NewHub() *Hub {
	return NewHub(func(event *HubEvent) {
		c.t.Logf("hub %s%v%s", Green, event.JSON(), Reset)
		if event.Clients == 0 {
			c.stop <- nil
		}
	})
}

func (c *control) addServer(client http.Handler, respF func(*IAttrs)) *TestWsServer {
	tws := NewTestWsServer(client, c.stop, respF)
	if tws != nil {
		c.wss = append(c.wss, tws)
	}
	return tws
}

func (c *control) Off(b bool) {
	for _, ws := range c.wss {
		ws.Off(b)
	}
}

func (c *control) Wait() {
	select {
	case err := <-c.stop:
		if err != nil {
			c.t.Errorf("%v", err)
		}
		return
	}
}

const (
	Red   = "\u001b[31m"
	Green = "\u001b[32m"
	Reset = "\u001b[0m"
)

// go test * -v

func Test_Simple(t *testing.T) {
	i1, i2, i3 := 1, 2, 3
	text1, text2 := "ok", "alarm"
	attrs1 := dom.AttrsFillText("#000", text1)
	attrs2 := dom.AttrsFillText("#f00", text2)

	control := newControl(t)
	hub := control.NewHub()
	view := hub.NewView(1)
	tx := func() { // send a single update
		view.Push(&dom.Push{
			IAttrs: dom.IAttrs{
				i1: attrs2, // update i1 with text2
			},
			Section: 0,
		})
	}
	rx := func(a *IAttrs) { // expect change and finish test
		defer control.Off(true)
		if a != nil {
			if got1, err := a.Text(i1); err != nil {
				t.Errorf("%v", err)
			} else if text2 != got1 {
				t.Errorf("%si1 expected text %s got %s%s", Red, text2, got1, Reset)
			} else {
				t.Logf("%si1 %v%s", Green, got1, Reset)
			}
			if got2, err := a.Text(i2); err != nil {
				t.Errorf("%s%v%s", Red, err, Reset)
			} else if text1 != got2 {
				t.Errorf("%si2 expected text %s got %s%s", Red, text1, got2, Reset)
			} else {
				t.Logf("%si2 %v%s", Green, got2, Reset)
			}
		}
	}

	v1 := newV1(view, dom.IAttrs{
		i1: attrs1,
		i2: attrs1,
		i3: attrs1,
	})
	client := NewClient(hub, v1)
	control.addServer(client, rx)
	tx()
	control.Wait()
}

func Test_100kIds(t *testing.T) {
	control := newControl(t)
	hub := control.NewHub()
	view := NewView(hub, 1)

	v1 := newV1(view, nil)
	size := 100000
	tx := func() { // send a lot of ids
		iattrs := dom.IAttrs{}
		for i := 1; i <= size; i++ { // start with id=1
			iattrs[i] = dom.AttrsFillText("#000", "text")
		}
		view.Response(&dom.Message{IAttrs: iattrs, Section: 0})
	}
	rx := func(a *IAttrs) { // expected the exact size of lot of ids
		defer control.Off(true)
		if a != nil {
			if got := len(a.Attrs); size != got {
				t.Errorf("%sexpected ids size %d got %d%s", Red, size, got, Reset)
			} else {
				t.Logf("ids %s%d%s", Green, got, Reset)
			}
		}
	}
	client := NewClient(hub, v1)
	control.addServer(client, rx)
	tx() // sent all ids
	control.Wait()
}

func Test_Clients(t *testing.T) {
	control := newControl(t)
	hub := control.NewHub()
	view1 := NewView(hub, 1)

	v1 := newV1(view1, nil)
	size := 1000 // number of ids updated
	tx := func() {
		iattrs := dom.IAttrs{}
		for i := 1; i <= size; i++ { // start with id=1
			iattrs[i] = dom.AttrsFillText("#000", "text")
		}
		view1.Response(&dom.Message{IAttrs: iattrs, Section: 0})
	}
	clients := 5 // number of browsers
	rxs := 0
	rx := func(a *IAttrs) {
		if a == nil {
			return
		}
		rxs += len(a.Attrs)
		if rxs == clients*size {
			// all clients receive all ids updated
			t.Logf("rxs %s%d%s", Green, rxs, Reset)
			control.Off(true)
		}
		// TODO timeout?
	}
	for i := 0; i < clients; i++ { // create multiple clients
		client := NewClient(hub, v1)
		control.addServer(client, rx)
	}
	tx() // sent to all browser once
	control.Wait()
}

// go test -run Test_Filtered * -v
func Test_Filtered(t *testing.T) {
	control := newControl(t)
	hub := control.NewHub()
	view := NewView(hub, 1)

	v2 := newV2(view, nil)
	size := 100000
	tx := func() { // send 100k ids
		iattrs := dom.IAttrs{}
		for i := 1; i <= size; i++ { // start with id=1
			iattrs[i] = dom.AttrsFillText("#000", "text")
		}
		view.Response(&dom.Message{IAttrs: iattrs, Section: 0})
	}
	expected := int(size / 2)
	rx := func(a *IAttrs) { // expected 50k ids since viewer2 is "halfer"
		defer control.Off(true)
		if a != nil {
			if got := len(a.Attrs); expected != got {
				t.Errorf("%sexpected ids size %d got %d%s", Red, expected, got, Reset)
			} else {
				t.Logf("ids %s%d%s", Green, got, Reset)
			}
		}
	}
	client := NewClient(hub, v2)
	control.addServer(client, rx)
	tx() // sent all ids
	control.Wait()
}
