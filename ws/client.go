package ws

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/websocket"

	"gitlab.com/jmireles/nav/dom"
)

// From "hub" https://github.com/gorilla/websocket/blob/master/examples/chat/client.go
const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second // 10 seconds
	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second // 60 seconds
	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10 // 54 seconds
	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

type Client struct {
	hub  *Hub
	page Page
	send chan []byte

	err    error
	conn   *websocket.Conn
	header http.Header

	id      int
	session int64
	prev    int
	section dom.Section
	click   *dom.Click
}

// viewer can be nil for simulators
func NewClient(hub *Hub, page Page) *Client {
	return &Client{
		hub:  hub,
		page: page,
		send: make(chan []byte, 1),
	}
}

// ServeHTTP implements http.Handler
// Useful to test clients
func (c *Client) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if conn, err := c.hub.upgrader.Upgrade(w, r, nil); err != nil {
		c.err = err
	} else {
		c.conn = conn
		c.header = r.Header
		c.hub.register <- c
		go c.writePump()
		go c.readPump()
	}
}

func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			away := websocket.CloseGoingAway
			abnormal := websocket.CloseAbnormalClosure
			if websocket.IsUnexpectedCloseError(err, away, abnormal) {
				//log.Printf("client.read error: %v", err)
			} else {
				// closing or refreshing a browser tab gives:
				// websocket: close 1001 (going away)
				//log.Printf("client.readPump %v", err)
			}
			// finish this goroutine and unregister from hub
			break
		}
		c.request(message)
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel
				//log.Printf("client.writePump closed message %v", message)
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func (c *Client) request(message []byte) {
	if c.page == nil {
		// clients without page (simulators) can't do requests
		return
	}
	req := struct {
		Go    string `json:"go"`
		Prefs string `json:"prefs"`
	}{}
	err := json.Unmarshal(message, &req)
	if err != nil {
		c.send <- []byte(`{"error":"` + err.Error() + `"}`)
		return
	}
	// 1) Prepare static texts (html,svg) to be sent to browser
	id, _ := strconv.Atoi(req.Go)
	c.click = &dom.Click{
		Id:    id,        // the id element clicked
		Prev:  c.prev,    // remember previous page
		Prefs: req.Prefs, // browser units,lang...
	}
	xml := &dom.XML{}
	e := ""
	c.section, err = c.page.PageDOM(c.click, xml)
	if err != nil {
		e = err.Error()
	}
	c.prev = c.click.Id
	c.send <- dom.NewResp(e, xml)

	// 2) Send to browser text elements attributes according section
	// for the first time. New attrs values will be passed later.
	c.response(nil) // elements states, colors, texts...
}

func (c *Client) response(src *View) {
	if c.page == nil {
		// prevent responses to pages without page (simulators)
		return
	}
	dst := c.page.PageView()
	if dst == nil {
		return // just in case no view was defined
	}
	if src != nil {
		if src.id != dst.id {
			// prevent send messages from one view to another not intended
			// that is, a browser page for view-1 is not interested in
			// view-2 updates and viceversa.
			return
		}
	}
	// scr nil means response is called from browser page creation
	// and we need to start with all the attrs states
	if filter := c.page.PageAttrsFilter(c.click, c.section); filter != nil {
		message := dst.filterJSON(filter)
		//if src != nil {
		//	log.Printf("client response src=%d dst=%d %s", src.id, dst.id, message)
		//}
		c.send <- []byte(message)
	}
}
