package ws

import (
	"gitlab.com/jmireles/nav/dom"
)

// Page implementation process a View.
//
type Page interface {
	// PageHtml returns the html associated to this page.
	PageHtml() *Html

	// PageView returns the view associated to this page.
	PageView() *View

	// PageDOM populates xml with a visible elements html/svg...
	// click has web page's section and preferences.
	// xml is the document to write in.
	PageDOM(*dom.Click, *dom.XML) (dom.Section, error)

	// PageAttrsFilter request the viewer attrs' so far.
	// click has web page's section and preferences.
	PageAttrsFilter(*dom.Click, dom.Section) PageFilterFunc
}

// PageFilterFunc is a function that receives an id an attrs and returns
// 1) nil when the page does not display the attrs
// 2) The same attrs when the page needs them as is or
// 3) A decorated copy of attrs with prefs when the page requires localization
type PageFilterFunc func(id int, attrs *dom.Attrs) *dom.Attrs

