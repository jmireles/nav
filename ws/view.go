package ws

import (
	"encoding/json"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/jmireles/nav/dom"
)

type Html struct {
	Tabs bool
}


// A View is a group of elements whose values updates can
// be published to web pages selected to a viewer type.
// Is tied to a websocket hub to update several pages.
// The pages can received a subgroup of updates using a filter
// to reduce innecesary changes.
type View struct {
	hub   *Hub
	id    int
	attrs map[int]*dom.Attrs
	last  dom.IAttrs
	mu    *sync.Mutex
}

func (v *View) Request(req interface{}) {
	if req == nil {
		return
	}
	go func() {
		if bytes, err := json.Marshal(req); err == nil {
			//log.Println("nav View.Req " + string(bytes))
			v.mu.Lock()
			v.hub.request(bytes)
			v.mu.Unlock()
		}
	}()
}

func (v *View) Push(resp *dom.Push) {
	if resp == nil {
		return
	}
	go func() {
		v.mu.Lock()
		// update this view attributes
		// calling hub response will bring
		// hub's clients here to filterJSON the
		// whole updated attribnutes
		for i, a := range resp.IAttrs {
			v.attrs[i] = v.last.Set(i, a)
		}
		v.hub.response(v) // was v.hub.refreshAll<- true
		v.mu.Unlock()
	}()
}

// FilterJSON returns a JSON string of all the attrs
// filtered with filterFunction. The filter checks the id
// and returns the original attrs, modified (units/lang) or nil
// String format returned{"attrs":{"id-1":{...},"id-2":{..},...}}
func (v *View) filterJSON(filter PageFilterFunc) string {
	v.mu.Lock()
	defer v.mu.Unlock()
	// Is better to build here the json so we don't need
	// a dom.IAttrs buffer where to collect all filter.
	// Is better to pass a copy of this buffer to attrsFiltered
	// to be converted to JSON.
	var sb strings.Builder
	sb.WriteString(`{"attrs":{`)
	var first = true
	for id, attrs := range v.attrs {
		if filtered := filter(id, attrs); filtered != nil {
			if first {
				first = false
			} else {
				sb.WriteString(",")
			}
			sb.WriteString(`"`)
			sb.WriteString(strconv.Itoa(id))
			sb.WriteString(`":`)
			filtered.JSON(&sb)
		}
	}
	sb.WriteString("}}")
	return sb.String()
}
