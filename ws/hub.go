package ws

import (
	"net"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/gorilla/websocket"

	"gitlab.com/jmireles/nav/dom"
)

type HubEvent struct {
	Clients    int
	Register   bool
	Page       bool
	Err        error
	LocalAddr  net.Addr
	RemoteAddr net.Addr
	Header     http.Header
}

func (e *HubEvent) JSON() string {
	var sb strings.Builder
	sb.WriteString("{")
	sb.WriteString(`"clients":` + strconv.Itoa(e.Clients))
	sb.WriteString(`,"register":` + strconv.FormatBool(e.Register))
	sb.WriteString(`,"page":` + strconv.FormatBool(e.Page))
	if e.Err != nil {
		sb.WriteString(`,"error":"` + e.Err.Error() + `"`)
	}
	if e.LocalAddr != nil {
		sb.WriteString(`,"local":"` + e.LocalAddr.String() + `"`)
	}
	if e.RemoteAddr != nil {
		sb.WriteString(`,"remote":"` + e.LocalAddr.String() + `"`)
	}
	if e.Header != nil {
		sb.WriteString(`,"header":` + strconv.Itoa(len(e.Header)))
	}
	sb.WriteString(`}`)
	return sb.String()
}

type HubEventFunc func(event *HubEvent)

// From "hub" https://github.com/gorilla/websocket/blob/master/examples/chat/hub.go
type Hub struct {
	upgrader websocket.Upgrader
	clients  map[*Client]bool

	register   chan *Client
	unregister chan *Client
	req        chan []byte
	resp       chan *View
	mu         *sync.Mutex
}

func NewHub(eventFunc HubEventFunc) *Hub {
	hub := &Hub{
		upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
		clients: make(map[*Client]bool),

		register:   make(chan *Client),
		unregister: make(chan *Client),
		req:        make(chan []byte, 1),
		resp:       make(chan *View, 1),
		mu:         &sync.Mutex{},
	}
	fireEvent := func(client *Client) {
		if eventFunc == nil {
			return
		}
		e := &HubEvent{
			Clients: len(hub.clients),
		}
		if client != nil { // client started
			e.Register = true
			e.Page = client.page != nil
			e.Err = client.err
			if client.conn != nil {
				// websocket error is nil
				e.LocalAddr = client.conn.LocalAddr()
				e.RemoteAddr = client.conn.RemoteAddr()
			}
			if client.header != nil {
				e.Header = client.header
			}
		} else { // client closed
			e.Register = false
		}
		eventFunc(e)
	}
	go func() {
		for {
			select {

			case client := <-hub.register:
				hub.clients[client] = true
				client.id = len(hub.clients)
				fireEvent(client)

			case client := <-hub.unregister:
				if _, ok := hub.clients[client]; ok {
					close(client.send)
					delete(hub.clients, client)
					fireEvent(nil)
				}

			case req := <-hub.req:
				for client := range hub.clients {
					if client.page == nil {
						// page=nil means this client should
						// attend requests because is a simulator
						select {
						case client.send <- req:
						default:
							close(client.send)
							delete(hub.clients, client)
							fireEvent(nil)
						}
					}
				}

			case view := <-hub.resp:
				for client := range hub.clients {
					client.response(view)
				}
			}
		}
	}()
	return hub
}

func (hub *Hub) NewView(id int) *View {
	v := &View{
		hub:   hub,
		id:    id,
		attrs: make(map[int]*dom.Attrs),
		last:  dom.IAttrs{},
		mu:    &sync.Mutex{},
	}
	return v
}

// response is protected to be called by views
// notifying the view has attributes updated of
// interest for hub's clients
func (h *Hub) response(view *View) {
	// previously h.resp<- was called directly by views'
	// which is incorrect since creates a race eventually.
	go func() {
		h.mu.Lock()
		h.resp <- view
		h.mu.Unlock()
	}()
}

func (h *Hub) request(bytes []byte) {
	go func() {
		h.mu.Lock()
		h.req <- bytes
		h.mu.Unlock()
	}()
}
