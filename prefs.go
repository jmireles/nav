package nav

import (
	"encoding/json"
	"io"
	"strings"

	"gitlab.com/jmireles/nav/dom"
)

// Units

type Units string

const (
	UnitsImperial = Units("i") // imperial default
	UnitsMetric   = Units("m") // metric
)

var unitsMap = map[Units]string{
	UnitsImperial: "imperial",
	UnitsMetric:   "metric",
}

// Lang

type Lang string

const (
	LangEn = Lang("en") // english (default)
	LangEs = Lang("es") // spanish
)

var langMap = map[Lang]string{
	LangEn: "english",
	LangEs: "spanish",
}

type Prefs struct {
	Units Units
	Lang  Lang
}

func NewPrefs(prefs string) (p *Prefs) {
	p = &Prefs{
		UnitsImperial,
		LangEn,
	}
	parts := strings.Split(prefs, ":")
	if len(parts) != 2 {
		return
	}
	switch parts[0] {
	case string(UnitsMetric):
		p.Units = UnitsMetric
	}
	switch parts[1] {
	case string(LangEs):
		p.Lang = LangEs
	}
	return
}

// Parse returns clicked id and preferences
func PrefsFromClick(c *dom.Click) (int, *Prefs) {
	if c == nil {
		return 0, nil
	}
	return c.Id, NewPrefs(c.Prefs)
}

var (
	// calls in templates nav.js
	prefsOnchangeUnits = dom.Aonchange(`prefs.change("units",this.value)`)
	prefsOnchangeLang  = dom.Aonchange(`prefs.change("lang",this.value)`)
	// this onclickWrite true means do a DOM location.reload()
	// to new page gets lang/units changes.
	prefsOnclickWrite = dom.Aonclick(`prefs.write("",true)`)
)

func PrefsHtml(body io.ReadCloser, closeIcon, unitsIcon, langIcon dom.Icon) string {
	x := &dom.XML{}
	p := struct {
		Go    string `json:"go"`
		Units string `json:"units"`
		Lang  string `json:"lang"`
	}{}
	if err := json.NewDecoder(body).Decode(&p); err != nil {
		x.Div(nil, "Invalid body attributes")
	}
	lang := LangEn
	if p.Lang == "es" {
		lang = LangEs
	}
	// check more languages here

	langsTr := func() {
		title := i18n.Prefs.Language.Get(lang)
		x.Td(dom.Atitle(title), func() {
			x.Icon(langIcon)
		})
		x.Td(nil, func() {
			x.Select(prefsOnchangeLang, func() {
				for l, v := range langMap {
					selected := p.Lang == string(l)
					x.Option(string(l), i18nPref(v, lang), selected)
				}
			})
		})
	}
	unitsTr := func() {
		title := i18n.Prefs.Units.Get(lang)
		x.Td(dom.Atitle(title), func() {
			x.Icon(unitsIcon)
		})
		x.Td(nil, func() {
			x.Select(prefsOnchangeUnits, func() {
				for u, v := range unitsMap {
					selected := p.Units == string(u)
					x.Option(string(u), i18nPref(v, lang), selected)
				}
			})
		})
	}
	x.Div(dom.Aclass("prefs"), func() {
		x.Div(nil, func() {
			title := i18n.Prefs.Close.Get(lang)
			x.Span(prefsOnclickWrite.Title(title), func() {
				x.Icon(closeIcon)
			})
		})
		x.Div(nil, i18n.Prefs.Preferences.Get(lang))
		x.Table([]func(){
			langsTr,
			unitsTr,
		})
	})
	return x.String()
}
