package nav

import (
	"embed"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/jmireles/nav/dom"
	"gitlab.com/jmireles/nav/ws"
)

//go:embed templates
var tpls embed.FS
var templates = NewTemplates(tpls, "templates/*") // will panic on errors

type Paths struct {
	Websocket string // templates/ws.js URL for new Websocket()
	PostPrefs string // templates/ws.js API URL for prefs.open()
	PostReq   string // templates/ws.js API URL for postReq()
}

func Html(r *http.Request, paths *Paths, css string, html *ws.Html) string {
	base := r.URL.Path
	data := struct { // templates data
		Clean     string   // base cleaned for local-storage
		AttrText  string   // DOM text
		AttrClass string   // DOM class
		AttrsKeys string   // DOM attrs mapping for go/javascript attributes
		Ids       *dom.Ids // negative constants DOM ids
		Paths     *Paths   // paths corrected with base
		Timeout   string
	}{
		Clean:     strings.Replace(base, "/", "-", -1),
		AttrText:  string(dom.AttrText),  // letter 't'
		AttrClass: string(dom.AttrClass), // letter 'c'
		AttrsKeys: dom.AttrsKeysJSON,
		Ids:       dom.NewIds(),
		Paths: &Paths{
			base + paths.Websocket,
			base + paths.PostPrefs,
			base + paths.PostReq,
		},
		Timeout: strconv.Itoa(5), // 5 seconds TODO configurable?
	}
	xml := &dom.XML{}
	return xml.HTML(func() {
		xml.Head(func() {
			xml.Style(func() {
				templates.Read(xml, "nav.css", nil)
				templates.Read(xml, "cfg.css", nil)
				xml.W0(css) // extra style
			})
			xml.Script(func() {
				// nav.js content:
				//
				// add localStorage clean prefix
				// const postReq = (values, params, resp)=> {...} send post to server

				// s is the websocket connected
				// n.o and n.c are counts openings/closings of server

				// rxT receives texts definitions. This way a big new svg
				// is set on browser elem with objects with server fixed ids
				// which later will update colors, texts, positions, curves...

				// rxA receives attributes values to change visual states
				// if key=="0" is for change the text of xml elem
				// the rest of keys are attributes mostly svg for colors, positions, points, etc

				// ok updates id=server element with green message
				// err updates id=server element with red message

				// conn is called by body script (see js code above)
				// and after closed timeout to auto-connection for returning servers/networks
				// s.onopen: A websockets opens by server, increase opens, clears closing
				// tx to server last go click or "1" for defautl
				// s.onerror: A websocket error occurs when server connection is lost. Notify browser
				// s.onmessageServer websocket json data. Call rx to detect texts or attrs
				// to update objects or colors, positions,...
				// s.onclose: Server websocket disconnected. Notify the error at head div, clear body.
				// Start a timer to restablish connection calling conn again. Default 5 seconds.

				// eventListener('click'): After User clicks over the page we find nearest element with class="go".
				// The id of such element is 1) Saved to local storage
				// and 2) send to server to be interpreted as user clicking action
				templates.Read(xml, "nav.js", data)
				templates.Read(xml, "cfg.js", data)
				if html != nil && html.Tabs {
					templates.Read(xml, "tabs.js", nil)
				}
			})
		})
		xml.Body(func() {
			templates.Read(xml, "nav-body.xml", data)
		})
	})
}
