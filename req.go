package nav

import (
	"errors"
	"strconv"
	"sync"
	"time"

	"gitlab.com/jmireles/nav/dom"
	"gitlab.com/jmireles/nav/ws"
)

type ReqSync struct {
	mu   *sync.Mutex
	busy bool
}

func NewReqSync() *ReqSync {
	return &ReqSync{mu: &sync.Mutex{}}
}

func (s *ReqSync) Lock() {
	s.mu.Lock()
}

func (s *ReqSync) Unlock() {
	s.mu.Unlock()
	s.busy = false
}

func (s *ReqSync) BusyPopup(post *ReqPost) *JsonResp {
	if s.busy {
		return NewJsonRespPopup("", errors.New(post.I18nBusy()))
	}
	s.busy = true
	return nil
}

type ReqPostSaveFunc func(req *JsonReq, extra []byte) error

type ReqPost struct {
	req   *JsonReq
	view  *ws.View
	save  ReqPostSaveFunc
	prefs *Prefs
}

func NewReqPost(req *JsonReq, view *ws.View, save ReqPostSaveFunc) *ReqPost {
	return &ReqPost{
		req:   req,
		view:  view,
		save:  save,
		prefs: NewPrefs(req.Prefs),
	}
}

func (p *ReqPost) Push(push *dom.Push) {
	if p.view != nil {
		p.view.Push(push)
	}
}

func (p *ReqPost) Req() *JsonReq {
	return p.req
}

func (p *ReqPost) Prefs() *Prefs {
	return p.prefs
}

func (p *ReqPost) XML() *dom.XML {
	return &p.req.XML
}

func (p *ReqPost) DomInt() (int, bool) {
	valInt, ok := p.req.DomInt()
	return valInt, ok
}

func (p *ReqPost) SaveTitleValues() (title, values string) {
	if p.req.Serial != nil {
		title = p.I18nSave()
		values = p.req.PostSaveSerial().Values()
	} else if p.req.All != 0 {
		title = p.I18nSaveAll()
		values = p.req.PostSaveAll().Values()
	} else {
		title = p.I18nSaveBox() + " " + strconv.Itoa(int(p.req.Id))
		values = p.req.PostSaveBox().Values()
	}
	return
}

func (p *ReqPost) SaveBoxOnclick() (i18n, onclick string) {
	i18n = p.I18nSaveBox() + " " + strconv.Itoa(int(p.req.Id))
	onclick = p.req.PostSaveBox().JavascriptOnclick()
	return
}

func (p *ReqPost) SaveAllOnclick() (i18n, onclick string) {
	i18n = p.I18nSaveAll()
	onclick = p.req.PostSaveAll().JavascriptOnclick()
	return
}

func (p *ReqPost) Save(commSet byte, extra [][]byte) {
	saveReq := &JsonReq{
		Net:     p.req.Net,
		Line:    p.req.Line,
		Command: commSet,
		Id:      p.req.Id,
		All:     p.req.All,
		Serial:  p.req.Serial,
	}
	for _, b := range extra {
		p.save(saveReq, b)
		time.Sleep(200 * time.Millisecond)
	}
}

func (p *ReqPost) I18nBusy() string {
	return i18n.Req.Busy.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nCancel() string {
	return i18n.Req.Cancel.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nConfirm() string {
	return i18n.Req.Confirm.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nRead() string {
	return i18n.Req.Read.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nReadAll() string {
	return i18n.Req.ReadAll.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nReading() string {
	return i18n.Req.Reading.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nSave() string {
	return i18n.Req.Save.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nSaveAll() string {
	return i18n.Req.SaveAll.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nSaveBox() string {
	return i18n.Req.SaveBox.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nSaved() string {
	return i18n.Req.Saved.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nSaving() string {
	return i18n.Req.Saving.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nTimeout() string {
	return i18n.Req.Timeout.Get(p.prefs.Lang)
}

func (p *ReqPost) I18nValues() string {
	return i18n.Req.Values.Get(p.prefs.Lang)
}
