package dom

import (
	"strconv"
	"strings"
)

type XML struct {
	sb strings.Builder
}

func (x *XML) W0(s string) {
	x.sb.WriteString(s)
}

func (x *XML) W(elem string, a A, inner interface{}) {
	x.sb.WriteString(`<` + elem)
	a.write(&x.sb)
	if inner == nil {
		x.sb.WriteString(`></` + elem + `>`)
		return
	}
	switch inner.(type) {
	case string:
		text := inner.(string)
		if text != "" {
			x.sb.WriteString(`>` + text + `</` + elem + `>`)
		} else {
			x.sb.WriteString(`/>`)
		}
	case func():
		f := inner.(func())
		x.sb.WriteString(`>`)
		f()
		x.sb.WriteString(`</` + elem + `>`)
	default:
		x.sb.WriteString(`?</` + elem + `>`)
	}
}

const (
	_body   = "body"
	_button = "button"
	_circle = "circle"
	_div    = "div"
	_head   = "head"
	_html   = "html"
	_g      = "g"
	_input  = "input"
	_path   = "path"
	_script = "script"
	_select = "select"
	_span   = "span"
	_svg    = "svg"
	_table  = "table"
	_tbody  = "tbody"
	_thead  = "thead"
	_td     = "td"
	_text   = "text"
	//_title   = "title" in dom.go
	_th    = "th"
	_tr    = "tr"
	_tspan = "tspan"
	_xmlns = "xmlns"
)

func (x *XML) Body(inner func()) {
	x.W(_body, nil, inner)
}

func (x *XML) Button(a A, text string) {
	x.W(_button, a, text)
}

func (x *XML) Circle(a A, inner interface{}) {
	x.W(_circle, a, inner)
}

func (x *XML) Div(a A, inner interface{}) {
	x.W(_div, a, inner)
}

func (x *XML) Head(inner func()) {
	x.W(_head, nil, inner)
}

// HTML returns an HTML text
func (x *XML) HTML(inner func()) string {
	x.W0("<!DOCTYPE html>\n")
	x.W(_html, nil, func() {
		x.W0("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n")
		x.W0("<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>\n")
		inner()
	})
	return x.String()
}

func (x *XML) Icon(icon Icon) {
	x.Svg(Aclass("icon").Viewbox(0, 0, 15, 15), func() {
		x.Path(Ad(string(icon)))
	})
}

func (x *XML) IconText(icon Icon, text string) {
	if string(icon) != "" {
		x.Icon(icon)
		if text != "" {
			x.Span(nil, text)
		}
	} else {
		x.Span(Aclass("icon"), text)
	}
}

func (x *XML) Input(a A) {
	x.W(_input, a, nil)
}

func (x *XML) G(a A, inner interface{}) {
	x.W(_g, a, inner)
}

func (x *XML) Option(value, text string, selected bool) {
	x.W0(`<option`)
	if selected {
		x.W0(` selected`)
	}
	x.W0(` value="` + value + `">`)
	x.W0(text)
	x.W0(`</option>`)
}

func (x *XML) Path(a A) {
	x.W(_path, a, nil)
}

func (x *XML) Select(a A, inner interface{}) {
	x.W(_select, a, inner)
}

func (x *XML) Script(inner func()) {
	x.W(_script, nil, inner)
}

func (x *XML) Span(a A, inner interface{}) {
	x.W(_span, a, inner)
}

func (x *XML) Style(inner func()) {
	x.W(_style, nil, inner)
}

func (x *XML) Svg(a A, inner func()) {
	a[_xmlns] = "http://www.w3.org/2000/html"
	a[_xmlns+":xlink"] = "http://www.w3.org/1999/xlink"
	x.W(_svg, a, inner)
}

// Table writes tables with no need to specify "table" nor "tr" elements.
// Use it only when both elements table and tr has no attributes.
func (x *XML) Table(inner interface{}) {
	switch inner.(type) {
	case []func():
		trs := inner.([]func())
		x.W(_table, nil, func() {
			for _, tr := range trs {
				x.Tr(tr)
			}
		})
	case func():
		f := inner.(func())
		x.W(_table, nil, f)
	}
}

func (x *XML) Tbody(a A, inner interface{}) {
	x.W(_tbody, a, inner)
}

func (x *XML) Thead(a A, inner interface{}) {
	x.W(_thead, a, inner)
}

func (x *XML) Td(a A, inner interface{}) {
	x.W(_td, a, inner)
}

func (x *XML) Title(text string) {
	x.W(_title, nil, text)
}

func (x *XML) Tr(inner func()) {
	x.W(_tr, nil, inner)
}

// TextMiddle puts a SVG text with dominant-baseline and text-anchor as "middle"
// vertical align:
// alignment-baseline="middle" works only for chrome
// dominant-baseline="middle" works both for chrome/firefox
func (x *XML) TextMiddle(fontSize, class, text string, id int) {
	a := AtextMiddleMiddle()
	if fontSize != "" {
		a[_fontSize] = fontSize
	}
	if class != "" {
		a[_class] = class
	}
	if id > 0 {
		a[_id] = strconv.Itoa(id)
	}
	x.W(_text, a, text)
}

func (x *XML) Text(a A, inner interface{}) {
	x.W(_text, a, inner)
}

func (x *XML) Th(inner interface{}) {
	x.W(_th, nil, inner)
}

func (x *XML) Tspan(A A, inner interface{}) {
	x.W(_tspan, A, inner)
}

// String returns the buffer as string and reset it
// in case further calls can start over with the buffer
func (x *XML) String() string {
	defer x.sb.Reset()
	return x.sb.String()
}
