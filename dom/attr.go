package dom

import (
	"strconv"
	"strings"
)

const ( // these keys should be unique
	_aText        = 't' // 0
	_aClass       = 'c' // 1
	_aFill        = 'f' // 2
	_aFontSize    = 'F' // 3
	_aStroke      = 's' // 4
	_aStrokeWidth = 'S' // 5
	_aTransform   = 'T' // 6
	_aD           = 'd' // 7 path "d"
	_aX           = 'x' // 8 x position
	_aY           = 'y' // 9 y position
)

// Passed to javascript code to convert single-letter Go Attribute to real DOM attribute.
const AttrText = _aText

const AttrClass = _aClass

const AttrsKeysJSON = `{
 "` + string(_aFill) + `":"` + _fill + `",
 "` + string(_aFontSize) + `":"` + _fontSize + `",
 "` + string(_aStroke) + `":"` + _stroke + `",
 "` + string(_aStrokeWidth) + `":"` + _strokeWidth + `",
 "` + string(_aTransform) + `":"` + _transform + `",
 "` + string(_aD) + `":"` + _d + `",
 "` + string(_aX) + `":"` + _x + `",
 "` + string(_aY) + `":"` + _y + `"
}`

type Attrs map[byte]string

func (a Attrs) JSON(sb *strings.Builder) {
	c := `` // comma
	sb.WriteString(`{`)
	if v, ok := a[_aText]; ok {
		sb.WriteString(c + `"` + string(_aText) + `":"` + v + `"`)
		c = `,`
	} // 0
	if v, ok := a[_aClass]; ok {
		sb.WriteString(c + `"` + string(_aClass) + `":"` + v + `"`)
		c = `,`
	} // 1
	if v, ok := a[_aFill]; ok {
		sb.WriteString(c + `"` + string(_aFill) + `":"` + v + `"`)
		c = `,`
	} // 2
	if v, ok := a[_aFontSize]; ok {
		sb.WriteString(c + `"` + string(_aFontSize) + `":"` + v + `"`)
		c = `,`
	} // 3
	if v, ok := a[_aStroke]; ok {
		sb.WriteString(c + `"` + string(_aStroke) + `":"` + v + `"`)
		c = `,`
	} // 4
	if v, ok := a[_aStrokeWidth]; ok {
		sb.WriteString(c + `"` + string(_aStrokeWidth) + `":"` + v + `"`)
		c = `,`
	} // 5
	if v, ok := a[_aTransform]; ok {
		sb.WriteString(c + `"` + string(_aTransform) + `":"` + v + `"`)
		c = `,`
	} // 6
	if v, ok := a[_aD]; ok {
		sb.WriteString(c + `"` + string(_aD) + `":"` + v + `"`)
		c = `,`
	} // 7
	if v, ok := a[_aX]; ok {
		sb.WriteString(c + `"` + string(_aX) + `":"` + v + `"`)
		c = `,`
	} // 8
	if v, ok := a[_aY]; ok {
		sb.WriteString(c + `"` + string(_aY) + `":"` + v + `"`)
		c = `,`
	} // 9
	sb.WriteString(`}`)
}

func AttrsText(text interface{}) Attrs {
	if text != nil {
		switch text.(type) {
		case string:
			return Attrs{
				_aText: text.(string),
			}
		case int:
			return Attrs{
				_aText: strconv.Itoa(text.(int)),
			}
		}
	}
	return Attrs{
		_aText: "",
	}
}

func AttrsTextClass(text interface{}, class string) Attrs {
	a := AttrsText(text)
	a[_aClass] = class
	return a
}

func AttrsFillStroke(fill, stroke string) Attrs {
	return Attrs{
		_aFill:   fill,
		_aStroke: stroke,
	}
}

func AttrsFillText(fill, text string) Attrs {
	return Attrs{
		_aFill: fill,
		_aText: text,
	}
}

func AttrsTextXY(text, x, y string) Attrs {
	return Attrs{
		_aText: text,
		_aX:    x,
		_aY:    y,
	}
}

func AttrsStrokeD(stroke, d string) Attrs {
	return Attrs{
		_aStroke: stroke,
		_aD:      d,
	}
}

func (attrs *Attrs) Text() string {
	return (*attrs)[_aText]
}

func (attrs *Attrs) Class() string {
	return (*attrs)[_aClass]
}

func (attrs *Attrs) Fill() string {
	return (*attrs)[_aFill]
}

func (attrs *Attrs) D() string {
	return (*attrs)[_aD]
}

func (attrs *Attrs) Transform() string {
	return (*attrs)[_aTransform]
}

func (attrs *Attrs) RotateSet(angle string) {
	(*attrs)[_aTransform] = "rotate(" + angle + ")"
}

// IAttrs is a map with keys integers
// and values Attrs
type IAttrs map[int]Attrs

func (e *IAttrs) Keys() []int {
	keys := make([]int, 0, len((*e)))
	for k := range *e {
		keys = append(keys, k)
	}
	return keys
}

func (e *IAttrs) JSON() string {
	keys := e.Keys()
	var sb strings.Builder
	sb.WriteString(`{`)
	for pos, k := range keys {
		if pos > 0 {
			sb.WriteString(",")
		}
		sb.WriteString(`"` + strconv.Itoa(k) + `":`)
		e.get(k).JSON(&sb)
	}
	sb.WriteString("}")
	return sb.String()
}

func (e *IAttrs) Set(id int, attrs Attrs) *Attrs {
	i := e.get(id)
	for k, v := range attrs {
		(*i)[k] = v
	}
	return i
}

func (e *IAttrs) get(id int) *Attrs {
	i, ok := (*e)[id]
	if !ok {
		(*e)[id] = make(Attrs, 0)
		i = (*e)[id]
	}
	return &i
}

func (a *AttrOld) fill(m *Attrs) {
	if a.Fill != "" {
		(*m)[_aFill] = a.Fill
	}
}

type AttrOld struct { // json used by sim/attrs/
	Text        string `json:"text,omitempty"`         // 0
	Class       string `json:"class,omitempty"`        // 1
	Fill        string `json:"fill,omitempty"`         // 2
	FontSize    string `json:"font-size,omitempty"`    // 3
	Stroke      string `json:"stroke,omitempty"`       // 4
	StrokeWidth string `json:"stroke-width,omitempty"` // 5
	Transform   string `json:"transform,omitempty"`    // 6
	D           string `json:"d,omitempty"`            // 7
	X           string `json:"x,omitempty"`            // 8
	Y           string `json:"y,omitempty"`            // 9
}

func (a *AttrOld) PathGet() *Attrs {
	m := make(Attrs, 0)
	a.pathSet(&m)
	return &m
}

func (a *AttrOld) pathSet(m *Attrs) {
	a.stroke(m)
	a.d(m)
}

func (a *AttrOld) FigGet() *Attrs {
	m := make(Attrs, 0)
	a.FigSet(&m)
	return &m
}

// SetFig only sets Fill, Stroke an StrokWidth attrs
func (a *AttrOld) FigSet(m *Attrs) {
	a.fill(m)
	a.stroke(m)
	a.strokeWidth(m)
}

func (a *AttrOld) TextGet() *Attrs {
	m := make(Attrs, 0)
	a.TextSet(&m)
	return &m
}

// SetTest only sets Fill, FontSize and Text attrs
func (a *AttrOld) TextSet(m *Attrs) {
	a.fill(m)
	a.fontSize(m)
	a.x(m)
	a.y(m)
	a.text(m)
}

func (a *AttrOld) d(m *Attrs) {
	(*m)[_aD] = a.D
}

func (a *AttrOld) fontSize(m *Attrs) {
	if a.FontSize != "" {
		(*m)[_aFontSize] = a.FontSize
	}
}

func (a *AttrOld) stroke(m *Attrs) {
	if a.Stroke != "" {
		(*m)[_aStroke] = a.Stroke
	}
}

func (a *AttrOld) strokeWidth(m *Attrs) {
	if a.StrokeWidth != "" {
		(*m)[_aStrokeWidth] = a.StrokeWidth
	}
}

func (a *AttrOld) text(m *Attrs) {
	if a.Text != "" {
		(*m)[_aText] = a.Text
	}
}

func (a *AttrOld) x(m *Attrs) {
	if a.X != "" {
		(*m)[_aX] = a.X
	}
}

func (a *AttrOld) y(m *Attrs) {
	if a.Y != "" {
		(*m)[_aY] = a.Y
	}
}
