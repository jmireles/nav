package dom

import (
	"encoding/json"
	"strings"
)

type Resp struct {
	Texts Texts  `json:"texts,omitempty"`
	Attrs IAttrs `json:"attrs,omitempty"`
}

func NewResp(err string, xml *XML) []byte {
	texts := NewTexts(Text(err), Text(xml.String()))
	resp := &Resp{Texts: texts}
	return resp.Bytes()
}

func (r *Resp) Bytes() []byte {
	bytes, err := json.Marshal(r)
	if err != nil {
		panic(err)
	}
	return bytes
}

type RespHead struct {
	sb *strings.Builder
}

func NewRespHead() *RespHead {
	return &RespHead{&strings.Builder{}}
}

func (r *RespHead) W(s string) {
	r.sb.WriteString(s)
}

func (r *RespHead) Text() Text {
	return Text(r.sb.String())
}

type Text string

type Texts map[int]Text

func NewTexts(head, body Text) Texts {
	return map[int]Text{
		idHead: head,
		idBody: body,
	}
}

type T struct {
	Text string `json:"text"`
}
