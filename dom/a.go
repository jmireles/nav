package dom

import (
	"sort"
	"strconv"
	"strings"
)

const (
	_auto             = "auto"
	_class            = "class"
	_d                = "d"
	_dominantBaseline = "dominant-baseline"
	_end              = "end"
	_fill             = "fill"
	_fontSize         = "font-size"
	_fontWeight       = "font-weight"
	_height           = "height"
	_id               = "id"
	_max              = "max"
	_middle           = "middle"
	_min              = "min"
	_name             = "name"
	_number           = "number"
	_onchange         = "onchange"
	_onclick          = "onclick"
	_rotate           = "rotate"
	_scale            = "scale"
	_start            = "start"
	_step             = "step"
	_style            = "style"
	_stroke           = "stroke"
	_strokeWidth      = "stroke-width"
	_textAnchor       = "text-anchor"
	_title            = "title"
	_transform        = "transform"
	_translate        = "translate"
	_type             = "type"
	_value            = "value"
	_viewbox          = "viewbox"
	_width            = "width"
	_x                = "x"
	_y                = "y"
)

type A map[string]string

// Note values are quoted simply '' not double ""
// because this text is sent to innerHTML DOMs
func (a *A) write(sb *strings.Builder) {
	keys := make([]string, 0)
	for k, v := range *a {
		if v != "" {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	for _, k := range keys {
		sb.WriteString(` ` + k + `='` + (*a)[k] + `'`)
	}
}

// Double quotes

func (a *A) JSON(quote string) string {
	var sb strings.Builder
	comma := ``
	sb.WriteString(`{`)
	for k, v := range *a {
		sb.WriteString(comma + k + `:` + quote + v + quote)
		comma = `,`
	}
	sb.WriteString(`}`)
	return sb.String()
}

// Aclass will create 'class'='A B C ...'
func Aclass(c ...string) A {
	return A{}.Class(c...)
}

// Common attributes factory
// Ad will create 'd'='A B C ...'
func Ad(d interface{}) A {
	return A{_d: aV(d)}
}

func Afill(fill string) A {
	return A{}.Fill(fill)
}

// Aid create 'id'='string/number'
func Aid(id interface{}) A {
	return A{}.Id(id)
}

// Aonchange creates 'onchange'='text'
func Aonchange(v string) A {
	return A{}.Onchange(v)
}

// Aonclick creates 'onclick'='text'
func Aonclick(v string) A {
	return A{}.Onclick(v)
}

// Astyle creates 'style'='text'
func Astyle(text string) A {
	return A{}.Style(text)
}

func Atext(text string) A {
	return A{}.Text(text)
}

func AtextAutoEnd() A {
	return A{_dominantBaseline: _auto, _textAnchor: _end}
}
func AtextAutoMiddle() A {
	return A{_dominantBaseline: _auto, _textAnchor: _middle}
}

func AtextAutoStart() A {
	return A{_dominantBaseline: _auto, _textAnchor: _start}
}
func AtextMiddleEnd() A {
	return A{_dominantBaseline: _middle, _textAnchor: _end}
}

func AtextMiddleMiddle() A {
	return A{_dominantBaseline: _middle, _textAnchor: _middle}
}

func AtextMiddleStart() A {
	return A{_dominantBaseline: _middle, _textAnchor: _start}
}

// Atitle creates 'title'='text'
func Atitle(text string) A {
	return A{}.Title(text)
}

func Atransform(t ...string) A {
	a := append([]string{}, t...)
	return A{_transform: strings.Join(a, " ")}
}

// Atypenumber creates 'type'='number' min='text' value='text' max='text' step='text'
func AtypeNumber(min, max, value, step string) A {
	a := A{_type: _number}
	if min != "" {
		a[_min] = min
	}
	if max != "" {
		a[_max] = max
	}
	if value != "" {
		a[_value] = value
	}
	if step != "" {
		a[_step] = step
	}
	return a
}

// Awh creates 'width'='number/text' 'height'='number/text'
func Awh(w, h interface{}) A {
	return A{
		_width:  aV(w),
		_height: aV(h),
	}
}

// Axy creates 'x'='number' 'y'='number'
func Axy(x, y int) A {
	return A{
		_x: strconv.Itoa(x),
		_y: strconv.Itoa(y),
	}
}

// Join returns a combined attributes
func (a A) Join(b A) A {
	c := A{}
	for k, v := range a {
		c[k] = v
	}
	for k, v := range b {
		c[k] = v
	}
	return c
}

func (a A) Class(c ...string) A {
	array := append([]string{}, c...)
	a[_class] = strings.Join(array, " ")
	return a
}

func (a A) Id(v interface{}) A {
	switch v.(type) {
	case string:
		a[_id] = v.(string)
	case int:
		a[_id] = strconv.Itoa(v.(int))
	}
	return a
}

func (a A) Fill(v string) A {
	a[_fill] = v
	return a
}

func (a A) Font(size, weight string) A {
	a[_fontSize] = size
	if weight != "" {
		a[_fontWeight] = weight
	}
	return a
}

func (a A) Name(v string) A {
	a[_name] = v
	return a
}

func (a A) Onchange(v string) A {
	a[_onchange] = v
	return a
}

func (a A) Onclick(v string) A {
	a[_onclick] = v
	return a
}

func (a A) Style(v string) A {
	a[_style] = v
	return a
}

func (a A) Stroke(color, width string) A {
	if color != "" {
		a[_stroke] = color
	}
	if width != "" {
		a[_strokeWidth] = width
	}
	return a
}

func (a A) Title(title string) A {
	a[_title] = title
	return a
}

func (a A) Text(text string) A {
	a[_text] = text
	return a
}

func (a A) Type(v string) A {
	a[_type] = v
	return a
}

func (a A) Viewbox(x, y, w, h int) A {
	a[_viewbox] = strings.Join([]string{
		strconv.Itoa(x),
		strconv.Itoa(y),
		strconv.Itoa(w),
		strconv.Itoa(h),
	}, " ")
	return a
}

func Atranslate(x, y interface{}) string {
	return _translate + `(` + aV(x) + ` ` + aV(y) + `)`
}

func Arotate(r interface{}) string {
	return _rotate + `(` + aV(r) + `)`
}

func Ascale(r interface{}) string {
	return _scale + `(` + aV(r) + `)`
}

func aV(v interface{}) string {
	if v != nil {
		switch v.(type) {
		case int:
			return strconv.Itoa(v.(int))
		case string:
			return v.(string)
		case []string:
			return strings.Join(v.([]string), " ")
		}
	}
	return ""
}

type As map[string]A

// Javascript returns string in javascript format of
// a map of attributes printed as javascript strings
func (as *As) JSON(quote string) string {
	var sb strings.Builder
	comma := ``
	sb.WriteString(`{`)
	for k, v := range *as {
		sb.WriteString(comma + k + `:` + v.JSON(quote))
		comma = `,`
	}
	sb.WriteString(`}`)
	return sb.String()
}
