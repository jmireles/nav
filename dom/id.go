package dom

import (
	"strconv"
)

type Id int

/**
0x      F           F          F           F            F
   ----------- ----------- ----------- ---------- ------------
      group           element                section
  +-----------+-----------------------+-----------------------+
  |19 18 17 16|15 14|13 12 11 10  9  8| 7  6  5  4  3  2  1  0|
  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
*/
func NewId(group, element, section byte) int {
	return int(group)<<16 + int(element)<<8 + int(section)
}

func IdIsGroup(id int, group byte) bool {
	return byte(id>>16) == group
}

func IdIsElement(id int, element byte) bool {
	return byte(id>>8) == element
}

func IdIsSection(id int, section byte) bool {
	return byte(id) == section
}

func IdElement(id int) byte {
	return byte(id >> 8)
}

const (
	idNone  Id = 0
	idConn     = -1
	idHead     = -2
	idBody     = -3
	idPrefs    = -4
	idPopup    = -5
)

type Ids struct {
	Conn  string
	Head  string
	Body  string
	Prefs string
	Popup string
}

func NewIds() *Ids {
	return &Ids{
		Conn:  strconv.Itoa(idConn),  // green/red square with reconnections/disconnection counter
		Head:  strconv.Itoa(idHead),  // top notification, mainly disconnections
		Body:  strconv.Itoa(idBody),  // where to put svg
		Prefs: strconv.Itoa(idPrefs), // prefs top page with lang,units...
		Popup: strconv.Itoa(idPopup), // popup for error/help...
	}
}
