package dom

import (
	"strconv"
	"strings"
)

type Icon string

type Icons map[string]Icon

type Section int

type Push struct {
	IAttrs  IAttrs
	Section Section
}

func NewPush(iAttrs IAttrs, section Section) *Push {
	return &Push{IAttrs: iAttrs, Section: section}
}

func (m *Push) String() string {
	var sb strings.Builder
	sb.WriteString(`{s:` + strconv.Itoa(int(m.Section)) + `,i:{`)
	comma1 := ``
	for k, v := range m.IAttrs {
		sb.WriteString(comma1 + strconv.Itoa(k) + `:{`)
		comma2 := ``
		for k, v := range v {
			sb.WriteString(comma2 + string(k) + `:'` + v + `'`)
			comma2 = `,`
		}
		sb.WriteString(`}`)
		comma1 = `,`
	}
	sb.WriteString(`}`)
	return sb.String()
}

func (m *Push) Append(other *Push) {
	if other == nil {
		return
	}
	if m.IAttrs == nil {
		m.IAttrs = IAttrs{}
	}
	for k, v := range other.IAttrs {
		m.IAttrs[k] = v
	}
}

type Click struct {
	Id    int
	Prev  int
	Prefs string
}

type Tab struct {
	Icon  Icon
	Text  string
	Id    int
	Title string
}
