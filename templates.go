package nav

import (
	"bytes"
	"embed"
	"text/template"

	"gitlab.com/jmireles/nav/dom"

	"gopkg.in/yaml.v2"
)

type Templates struct {
	t *template.Template
}

func NewTemplates(e embed.FS, base string) *Templates {
	templates, err := template.ParseFS(e, base)
	if err != nil {
		panic(err)
	}
	return &Templates{templates}
}

func (t *Templates) Read(xml *dom.XML, file string, object interface{}) {
	var buff bytes.Buffer
	if err := t.t.ExecuteTemplate(&buff, file, object); err != nil {
		panic(err)
	} else {
		xml.W0(buff.String())
	}
}

func TemplateYamlUnmarshal(bytes []byte, i18n interface{}) error {
	return yaml.Unmarshal(bytes, i18n)
}
