package nav

import (
	"strconv"
	"strings"

	"gitlab.com/jmireles/nav/dom"
)

type JsonPost struct {
	values []string
	params string
	resp   []string
}

func (j *JsonPost) Values() string {
	return `{` + strings.Join(j.values, ",") + `}`
}

func (j *JsonPost) Resp() string {
	return `{` + strings.Join(j.resp, ",") + `}`
}

// Returns javascript method and parameters for post a request from DOM elements
// Javascript method is called "postReq" and is defined in templates/ws.js file.
// javascript method calls server with x.open("POST", "{{ .Base }}/req", true);
// and will include browser preferences for server translations and units management.
// to be catched by an API which will parse JSON payload with this go JsonReq struct. See below.
// first parameter is values json map simple query.
// second parameter is params json array.
// third parameter is resp json map.
// fourth parameter is 'this' to return DOM id, data-int and innerHTML
func (j *JsonPost) JavascriptOnclick() string {
	return `postReq(` + j.Values() + `,` + j.params + `,` + j.Resp() + `,this)`
}

func JsonPostGet(net, line, command, box byte, key, value string) *JsonPost {
	values := []string{
		`n:` + strconv.Itoa(int(net)),
		`l:` + strconv.Itoa(int(line)),
		`c:` + strconv.Itoa(int(command)),
		`i:` + strconv.Itoa(int(box)),
	}
	resp := make([]string, 0)
	if key != "" && value != "" {
		resp = append(resp, `innerHTML:{"`+key+`":"`+value+`"}`)
	}
	return &JsonPost{values, `[]`, resp}
}

func JsonPostSerialEdit(net, line, command, box byte, serial []byte, g, f int) *JsonPost {
	ss := make([]string, len(serial))
	for pos, s := range serial {
		ss[pos] = strconv.Itoa(int(s))
	}
	values := []string{
		`n:` + strconv.Itoa(int(net)),
		`l:` + strconv.Itoa(int(line)),
		`c:` + strconv.Itoa(int(command)),
		`i:` + strconv.Itoa(int(box)),
		`s:[` + strings.Join(ss, ",") + `]`,
		`g:` + strconv.Itoa(g),
		`f:` + strconv.Itoa(f),
		`p:` + strconv.Itoa(jsonPopupEdit), // p=2 is popup-edit
	}
	resp := make([]string, 0)
	return &JsonPost{values, `[]`, resp}
}

func JsonPostSerial(net, line, command, box byte, serial []byte, g int) *JsonPost {
	ss := make([]string, len(serial))
	for pos, s := range serial {
		ss[pos] = strconv.Itoa(int(s))
	}
	values := []string{
		`n:` + strconv.Itoa(int(net)),
		`l:` + strconv.Itoa(int(line)),
		`c:` + strconv.Itoa(int(command)),
		`i:` + strconv.Itoa(int(box)),
		`s:[` + strings.Join(ss, ",") + `]`,
		`g:` + strconv.Itoa(int(g)),
	}
	resp := make([]string, 0)
	return &JsonPost{values, `[]`, resp}
}

func JsonPostHelp(req *JsonReq, g, f int) *JsonPost {
	values := []string{
		`l:` + strconv.Itoa(int(req.Line)),    // l is json name in JsonReq.Line
		`c:` + strconv.Itoa(int(req.Command)), // c is json name in JsonReq.Command
		`i:` + strconv.Itoa(int(req.Id)),      // i is json name in JsonReq.Id
		`g:` + strconv.Itoa(g),                // g is json name in JsonReq.Group
		`f:` + strconv.Itoa(f),                // f is json name in JsonReq.Field
		`p:` + strconv.Itoa(jsonPopupHelp),    // p=1 is popup-help
	}
	return &JsonPost{values, `[]`, nil}
}

func JsonPostEdit(req *JsonReq, g, f int) *JsonPost {
	values := []string{
		`l:` + strconv.Itoa(int(req.Line)),    // l is json name in JsonReq.Line
		`c:` + strconv.Itoa(int(req.Command)), // c is json name in JsonReq.Command
		`i:` + strconv.Itoa(int(req.Id)),      // i is json name in JsonReq.Id
		`g:` + strconv.Itoa(g),                // g is json name in JsonReq.Group
		`f:` + strconv.Itoa(f),                // f is json name in JsonReq.Field
		`p:` + strconv.Itoa(jsonPopupEdit),    // p=2 is popup-edit
	}
	return &JsonPost{values, `[]`, nil}
}

const (
	jsonPopupHelp = iota + 1
	jsonPopupEdit
	jsonPopupSave
	jsonPopupShow
)

type JsonReq struct {
	Net     byte `json:"n"`
	Line    byte `json:"l"`
	Command byte `json:"c"`
	Id      byte `json:"i"`
	All     byte `json:"a"`
	Popup   byte `json:"p"`

	Serial []int `json:"s"`
	Group  int   `json:"g"`
	Field  int   `json:"f"`

	Values map[string]int `json:"values"` // javascript sets param(s) in DOM go get from savings

	DOM struct {
		Id   string `json:"id"`
		Int  string `json:"int"`
		Text string `json:"text"`
	} `json:"dom"`

	Prefs  string            `json:"prefs"` // added by javascript postReq
	Params map[string]string `json:"params"`

	XML dom.XML
}

func (j *JsonReq) SerialString(separator string) string {
	ss := make([]string, len(j.Serial))
	for pos, s := range j.Serial {
		ss[pos] = strconv.Itoa(int(s))
	}
	return strings.Join(ss, separator)
}

func (j *JsonReq) PostGet() *JsonPost {
	values := []string{
		`l:` + strconv.Itoa(int(j.Line)),
		`c:` + strconv.Itoa(int(j.Command)),
		`i:` + strconv.Itoa(int(j.Id)),
	}
	return &JsonPost{values, `[]`, nil}
}

func (j *JsonReq) PostSaveBox() *JsonPost {
	return &JsonPost{j.saveValues(0), `[]`, nil}
}

func (j *JsonReq) PostSaveAll() *JsonPost {
	return &JsonPost{j.saveValues(1), `[]`, nil}
}

func (j *JsonReq) PostSaveSerial() *JsonPost {
	values := j.saveValues(0)
	ss := make([]string, len(j.Serial))
	for pos, s := range j.Serial {
		ss[pos] = strconv.Itoa(int(s))
	}
	values = append(values, `n:`+strconv.Itoa(int(j.Net))) // net is needed for serials!!!
	values = append(values, `s:[`+j.SerialString(",")+`]`)
	return &JsonPost{values, `[]`, nil}
}

func (j *JsonReq) saveValues(all int) []string {
	return []string{
		`l:` + strconv.Itoa(int(j.Line)),    // l is json name in JsonReq.Line
		`c:` + strconv.Itoa(int(j.Command)), // c is json name in JsonReq.Command
		`i:` + strconv.Itoa(int(j.Id)),      // i is json name in JsonReq.Id
		`a:` + strconv.Itoa(int(all)),       // a=0 is normal, a=1 is broadcast
		`g:` + strconv.Itoa(j.Group),        // group
		`f:` + strconv.Itoa(j.Field),        // field
		`p:` + strconv.Itoa(jsonPopupSave),  // p=3 is popup-save
	}
}

func (r *JsonReq) PopupHelp() bool {
	return r.Popup == jsonPopupHelp
}

func (r *JsonReq) PopupEdit() bool {
	return r.Popup == jsonPopupEdit
}

func (r *JsonReq) PopupSave() bool {
	return r.Popup == jsonPopupSave && len(r.Values) == 0
}

func (r *JsonReq) Saved() bool {
	return r.Popup == jsonPopupSave && len(r.Values) > 0
}

func (r *JsonReq) DomInt() (int, bool) {
	if number, err := strconv.ParseInt(r.DOM.Int, 10, 0); err == nil {
		return int(number), true
	}
	return 0, false
}

type JsonResp struct {
	Resp  string
	err   string
	popup string
}

func (resp *JsonResp) Json() []byte {
	if resp.err != "" {
		return []byte(`{"error":"` + resp.err + `"}`)
	} else if resp.popup != "" {
		return []byte(`{"popup":"` + resp.popup + `"}`)
	} else {
		return []byte(resp.Resp)
	}
}

func (resp *JsonResp) Popup() bool {
	return resp.err != "" || resp.popup != ""
}

func NewJsonRespPopup(popup string, err error) *JsonResp {
	if err != nil {
		return &JsonResp{err: err.Error()}
	} else {
		return &JsonResp{popup: popup}
	}
}

func JsonError(err interface{}) []byte {
	switch err.(type) {
	case string:
		return []byte(`{"error":"` + err.(string) + `"}`)
	case error:
		return []byte(`{"error":"` + err.(error).Error() + `"}`)
	}
	return []byte(`{"error":"?"}`)
}
