package nav

import (
	_ "embed"
)

type I18Ns struct {
	En string `yaml:"en"`
	Es string `yaml:"es"`
	// add her more languages
}

func (i *I18Ns) Get(lang Lang) string {
	switch lang {
	case LangEs:
		if s := i.Es; s != "" {
			return s
		}
	}
	return i.En
}

type I18NsArray struct {
	En []string `yaml:"en"`
	Es []string `yaml:"es"`
}

func (i *I18NsArray) Get(lang Lang) []string {
	switch lang {
	case LangEs:
		if s := i.Es; s != nil {
			return s
		}
	}
	return i.En
}

//go:embed templates/nav-i18n.yaml
var i18nYaml []byte
var i18n i18N

func init() {
	if err := TemplateYamlUnmarshal(i18nYaml, &i18n); err != nil {
		panic(err)
	}
}

type i18N struct {
	Prefs i18nPrefs `yaml:"prefs"`
	Req   i18nReq   `yaml:"req"`
}

type i18nPrefs struct {
	Close       I18Ns `yaml:"close"`
	Units       I18Ns `yaml:"units"`
	Imperial    I18Ns `yaml:"imperial"`
	Metric      I18Ns `yaml:"metric"`
	Language    I18Ns `yaml:"language"`
	Preferences I18Ns `yaml:"preferences"`

	English I18Ns `yaml:"english"`
	Spanish I18Ns `yaml:"spanish"`
	// add here more languages
}

func (i *i18nPrefs) get(key string, lang Lang) string {
	switch key {
	case "close":
		return i.Close.Get(lang)
	case "units":
		return i.Units.Get(lang)
	case "imperial":
		return i.Imperial.Get(lang)
	case "metric":
		return i.Metric.Get(lang)
	case "language":
		return i.Language.Get(lang)
	case "english":
		return i.English.Get(lang)
	case "spanish":
		return i.Spanish.Get(lang)
	// add here more languages
	default:
		return "(" + key + ")"
	}
}

func i18nPref(key string, lang Lang) string {
	return i18n.Prefs.get(key, lang)
}

type i18nReq struct {
	Busy    I18Ns `yaml:"busy"`
	Cancel  I18Ns `yaml:"cancel"`
	Confirm I18Ns `yaml:"confirm"`
	Read    I18Ns `yaml:"read"`
	ReadAll I18Ns `yaml:"readAll"`
	Reading I18Ns `yaml:"reading"`
	Save    I18Ns `yaml:"save"`
	SaveAll I18Ns `yaml:"saveAll"`
	SaveBox I18Ns `yaml:"saveBox"`
	Saved   I18Ns `yaml:"saved"`
	Saving  I18Ns `yaml:"saving"`
	Timeout I18Ns `yaml:"timeout"`
	Values  I18Ns `yaml:"values"`
}
